<?php

/**
 * @file
 * Framework Nucleo
 * Todo codigo de Nucleo esta lanzando bajo MIT-GPL License.
 * https://gitlab.com/wappcom/nucleo.3
 * Creator: @hermany
 */
header('Content-Type: text/html; charset=utf8');

require_once("config.php");

require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

$fmt->auth->index();

