<?php
$pubs = explode(",", $pubAttrId);
$count = count ($pubs);
 

$returnHtml .=  '<!-- pub pub-block-magik-1 -->';
$returnHtml .=  '<div class="pub pub-block-magik pub-block-magik-1" pubid="pub-block-magik-1" module="content">
    <div class="pub-inner container">';

for ($i=0; $i < $count; $i++) { 
    $arrayPub = $this->fmt->pubs->publicationDataId($pubs[$i]);
    //var_dump($arrayPubs[$i]);
    $pub_type = $arrayPub["type"];
    $pub_path = $arrayPub["path"];
    $pubName = $arrayPub["name"];
    $pubAttrId = $arrayPub["attrId"];
    $pubTitle = $arrayPub["title"];
    $pubClass = $arrayPub["cls"];
    $pubPathUi = $arrayPub["pathUi"];

    if ($pub_type == 0) {
        $root = _PATH_HOST;
    } else {
        $root = _PATH_NUCLEO;
    }
    //var_dump($root . $pub_path);
    $returnHtml .= '<div class="section section-'.$i.'">';

    if ($this->fmt->files->existFile($root . $pub_path)) {
        require($root . $pub_path);
    } else {
        $returnHtml .= 'Error. No File :' . $pub_path;
    }
                
    $returnHtml .= '</div>';
}

$returnHtml .= '    </div>';
$returnHtml .= '</div>';