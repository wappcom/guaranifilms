<?php

require_once(_PATH_NUCLEO . "modules/inventory/controllers/class/class.products.php");
$products = new PRODUCTS($this->fmt);

$html = file_get_contents(_PATH_HOST . "sites/default/views/categoryList.html");

$html = str_replace("{{_ID}}", "ListCategory", $html);
$html = str_replace("{{_NAME}}", "Articulos COVID-19", $html);
$html = str_replace("{{_LINK}}", "articulos-convid-19", $html);
$html =  $this->fmt->setUrlNucleo($html);

$itemTmp = file_get_contents(_PATH_HOST . "sites/default/views/item.html");

$listProductsCatArray = $products->listProductsCatId(14);
$numList = count($listProductsCatArray);
//var_dump($listProductsCatArray);

$strItems = '';

for ($i = 0; $i < 5; $i++) {
    $id = $listProductsCatArray[$i]["id"];
    $name = $listProductsCatArray[$i]["name"];
    $price = $listProductsCatArray[$i]["price"];
    $img = $listProductsCatArray[$i]["img"];

    $item = str_replace("{{_ITEM}}", $id, $itemTmp);
    $item = str_replace("{{_NAME}}", $name, $item);
    $item = str_replace("{{_PRICE}}", $products->getOptionsProducts("coin") . $price, $item);
    $item = str_replace("{{_ITEM_PRICE}}", $price, $item);
    $item = str_replace("{{_TYPE_MONEY}}", $products->getOptionsProducts("coin"), $item);
    $item = str_replace("{{_IMAGEN}}", $this->fmt->files->urlAdd($img, "-thumb"), $item);
    $item = str_replace("{{_PATH_FILE}}", $img, $item);
    $strItems .= $item;
}


$html = str_replace("{{_ITEMS}}", $strItems, $html);

// por marcas 
$str = file_get_contents(_PATH_HOST . "sites/default/views/marks.html");
$str = str_replace("{{_ID}}", "pubMarks", $str);
$str = str_replace("{{_NAME}}", "Por Marcas", $str);
$str = str_replace("{{_PUB_CLASS}}", "pub-marks", $str);

$listaBrands = $products->listBrands();
$numListBrands = count($listaBrands);
$strItemsBrands = '';

for ($i = 0; $i < $numListBrands; $i++) {
    $id = $listaBrands[$i]["id"];
    $name = $listaBrands[$i]["name"];
    $logo = _PATH_FILES . $this->fmt->files->urlAdd($listaBrands[$i]["img"], "-thumb");
    $strItemsBrands .= '<div class="item item-' . $id . '"><a class="btnBrand" nane="' . $name . '" item="' . $id . '" ><img src="' .  $logo . '" ></i></a></div>';
}

$str = str_replace("{{_ITEMS}}", $strItemsBrands, $str);
$str =  $this->fmt->setUrlNucleo($str);

$html .= $str;
 
$returnHtml .=  $html;