<?php

$htmlWiew = file_get_contents(_PATH_HOST. "sites/default/views/contentId.html");
$arrayContent = $this->fmt->contents->dataId($pubAttrId);

//var_dump($arrayContent);

$htmlSet = $this->fmt->pubs->set($htmlWiew,$arrayPub);

$listMedia = $arrayContent["media"];
$list = '';
if ($listMedia!=0){
    $count = count($listMedia);
    $item = file_get_contents(_PATH_HOST . "sites/default/views/contentItemLinkMedia.html");
    for ($i = 0; $i < $count; $i++) {
        $htmlItem = $this->fmt->setUrlNucleo($item);
        $htmlItem = str_replace("{{_ITEM}}", "", $htmlItem);
        $htmlItem = str_replace("{{_TITLE}}", $listMedia[$i]["title"], $htmlItem);
        $htmlItem = str_replace("{{_SUMMARY}}", '<span class="summary">'.$listMedia[$i]["summary"].'</span>', $htmlItem);
        $htmlItem = str_replace("{{_PATH_URL_IMG}}", $listMedia[$i]["pathurl"], $htmlItem);
        $htmlItem = str_replace("{{_URL}}", $listMedia[$i]["url"], $htmlItem);
        $htmlItem = str_replace("{{_TARGET}}", $listMedia[$i]["target"], $htmlItem);
        $list .= $htmlItem;
    }
}

$htmlPub = str_replace("{{_TITLE}}", $arrayContent["title"], $htmlSet);
$htmlPub = str_replace("{{_SUMMARY}}", $arrayContent["summary"], $htmlPub);
$htmlPub = str_replace("{{_LIST_CLASS}}", "listImagesLinks", $htmlPub);
$htmlPub = str_replace("{{_LIST_ID}}", "listImagesLinks".$pubAttrId, $htmlPub);
$htmlPub = str_replace("{{_LIST}}", $list, $htmlPub);

$returnHtml .=  $htmlPub;
 