<?php
require_once('../../../../config.php');
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();
$data = $fmt->pubs->dataPOST($_POST['data']);
$arrayPub = $data['pub'];
$pubId = $data['pub']['id'];
$pubAttrId = $data['pub']['attrId'];
$pubJson = $data['pub']['json'];
$pubCls = $data['pub']['cls'];
$catId =  $data['catId']; 

define("_VS", "v".$fmt->options->versionPlus());

//$arrayCategorys = $this->fmt->categorys->nodes(67,1);
$arrayCategorys =$fmt->contents->relationCategory(array("catId"=> '67', "order" => 'cont_id ASC' ));

//var_dump($arrayCategorys);

$aux = [];
$count = count($arrayCategorys);
for ($i=0; $i < $count ; $i++) { 
    $aux[$i]['id'] = $arrayCategorys[$i]['id'];
    $aux[$i]['title'] = $arrayCategorys[$i]['title'];
    $aux[$i]['pathurl'] = $arrayCategorys[$i]['pathurl'];
}

//var_dump($aux);

$directorsScript = '<script>  _DIRECTORS = JSON.parse(`'.json_encode($aux).'`); 
    //console.log(_DIRECTORS);
    let menuDirectors =  _DIRECTORS.map(director => {
        return `
            <a class="btnDirector" path="${director.pathurl}"><span>${director.title}</span></a>
        `;
    });
    $("#menuDirectors").html( menuDirectors.join("") );
    setTimeout(() => {
        $("#menuDirectors").addClass("active");
    }, 300); 
</script>';
 

$social = file_get_contents(_PATH_HOST."sites/default/views/social.html");
$social= $fmt->setUrlNucleo($social);

$htmlNav = file_get_contents(_PATH_HOST."sites/default/views/video.html");
$htmlNav= str_replace("{{_LINK_IMG}}",_PATH_FILES."sites/default/assets/img/bg-home.jpg",$htmlNav);
$htmlNav= str_replace("{{_PATH_WEB}}",_PATH_WEB,$htmlNav);
$htmlNav= str_replace("{{_PATH_FILES}}",_PATH_FILES,$htmlNav);
$htmlNav= str_replace("{{_DIRECTORS}}",$directorsScript,$htmlNav);
$htmlNav= str_replace("{{_LINK_VIDEO}}",_PATH_FILES."sites/default/assets/video/portada-web-cabruja.mp4",$htmlNav);
$htmlNav= str_replace("{{_SOCIAL}}",$social,$htmlNav);

echo  $htmlNav;