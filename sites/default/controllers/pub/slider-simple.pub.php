<?php
require_once('../../../../config.php');
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();
$data = $fmt->pubs->dataPOST($_POST['data']);
$arrayPub = $data['pub'];
$pubId = $data['pub']['id'];
$pubAttrId = $data['pub']['attrId'];
$pubJson = $data['pub']['json'];
$pubCls = $data['pub']['cls'];
$catId =  $data['catId']; 

$returnHtml = '';

define("_VS", "v".$fmt->options->versionPlus());
$html = file_get_contents(_PATH_HOST."sites/default/views/slider-simple.html");
$html= $fmt->setUrlNucleo($html);
$html= str_replace("{{_VS}}",_VS,$html);

$returnHtml .=  $html;



$html = file_get_contents(_PATH_HOST."sites/default/views/about.html");
$html= $fmt->setUrlNucleo($html);
$html= str_replace("{{_VS}}",_VS,$html);
$html= str_replace("{{_PUB_CLASS}}","pub-about",$html);
$html= str_replace("{{_PUB_ID}}","pubAbout",$html);
$html= str_replace("{{_BODY}}","",$html);
$html= str_replace("{{_TITLE}}","EXPERIENCIA EN EL MERCADO
Y MIRADA CINEMATOGRÁFICA",$html);
$html= str_replace("{{_SUMMARY}}","
<p>Somos un grupo de productores y directores con más de 10 años de experiencia en sector audiovisual publicitario y cinematográfico, esto nos 
permite entender el mercado desde el punto de vista comercial y estético para así poder conformar un equipo de trabajo óptimo que asegure una 
producción de alta calidad, manteniendo un balance entre las variables: Precio, Calidad y Tiempo.</p>

<p>Nuestros directores y staff se mantienen en la vanguardia del cine publicitario paraguayo, acompañando las nuevas tendencias estéticas que 
surgen en el mercado internacional y nacional, garantizando así la producción de piezas audiovisuales con una identidad propia que aporten a los 
objetivos de nuestros clientes, y le den un valor agregado a sus campañas más allá que mirada exclusivamente comercial.</p>

",$html);


$returnHtml .=  $html;


$html = file_get_contents(_PATH_HOST."sites/default/views/services.html");
$html= $fmt->setUrlNucleo($html);
$html= str_replace("{{_VS}}",_VS,$html);
$html= str_replace("{{_PUB_CLASS}}","pub-services",$html);
$html= str_replace("{{_PUB_ID}}","pubServices",$html);
$html= str_replace("{{_TITLE}}","Nuestros Servicios",$html);


$returnHtml .=  $html;


$html = file_get_contents(_PATH_HOST."sites/default/views/partnerships.html");
$html= $fmt->setUrlNucleo($html);
$html= str_replace("{{_VS}}",_VS,$html);
$html= str_replace("{{_PUB_CLASS}}","pub-partnerships",$html);
$html= str_replace("{{_PUB_ID}}","pubPartnerships",$html);
$html= str_replace("{{_TITLE}}","",$html);


$returnHtml .=  $html;


$arrayCategorysLocal =$fmt->contents->relationCategory(array("catId"=> '85', "order" => " cont_title ASC" ));
$arrayCategorysExt =$fmt->contents->relationCategory(array("catId"=> '86' , "order" => " cont_title ASC" ));


$contLocal = count($arrayCategorysLocal);
$auxLocal = '';
for ($i=0; $i < $contLocal; $i++) { 
    $director = $arrayCategorysLocal[$i];
    $auxLocal .= '<div class="btnModDirector local" 
                path="'.$director["pathurl"].'" 
                style="background: url('._PATH_FILES.$director["img"]["pathurl"].') no-repeat center center;"
                item="'.$director["id"].'" >
                <span>'.$director["title"].'</span>
                <div class="bg"></div>
                <div class="video" item="'.$director["id"].'">
                    <a class="btnCancel" item="'.$director["id"].'"><i class=" icon icon-close"></i> <span>Cerrar</span> </a>
                    '.$director["body"].'
                </div>
            </div>';
}
 

$contExt = count($arrayCategorysExt);
$auxExt = '';
for ($i=0; $i < $contExt; $i++) { 
    $director = $arrayCategorysExt[$i];
    $auxExt .= '<div class="btnModDirector local" 
                path="'.$director["pathurl"].'" 
                style="background: url('._PATH_FILES.$director["img"]["pathurl"].') no-repeat center center;"
                item="'.$director["id"].'" >
                <span>'.$director["title"].'</span>
                <div class="bg"></div>
                <div class="video" item="'.$director["id"].'">
                    <a class="btnCancel" item="'.$director["id"].'"><i class=" icon icon-close"></i> <span>Cerrar</span> </a>
                    '.$director["body"].'
                </div>
            </div>';
}

$html = file_get_contents(_PATH_HOST."sites/default/views/directorsLocals.html");
$html= $fmt->setUrlNucleo($html);
$html= str_replace("{{_VS}}",_VS,$html);
$html= str_replace("{{_PUB_CLASS}}","pub-directors",$html);
$html= str_replace("{{_PUB_ID}}","pubDirectors",$html);
$html= str_replace("{{_BODY_LOCALES}}",$auxLocal,$html);
$html= str_replace("{{_BODY_EXT}}",$auxExt,$html);



$returnHtml .=  $html.$directorsMixScript;


$html = file_get_contents(_PATH_HOST."sites/default/views/footerPage.html");
$html= $fmt->setUrlNucleo($html);
$html= str_replace("{{_VS}}",_VS,$html);
$html= str_replace("{{_PUB_CLASS}}","pub-footer",$html);
$html= str_replace("{{_PUB_ID}}","pubFooter",$html);
$html= str_replace("{{_SOCIAL}}",$social,$html);


$returnHtml .=  $html;

echo $returnHtml;