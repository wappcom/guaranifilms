<?php
require_once('../../../../config.php');
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();


$htmlNav = file_get_contents(_PATH_HOST . "sites/default/views/nav.html");
$htmlNav = str_replace("{{_PATH_WEB}}", _PATH_WEB, $htmlNav);
//$arrayNav =  $this->fmt->categorys->nodes();
$arrayNav =  $fmt->menus->items(1);
$count = count($arrayNav);

//var_dump($arrayNav);

//$navHtml = '<script src="'._PATH_WEB.'sites/default/components/facebookConector.js"></script>';
//$navHtml .= '<script src="'._PATH_WEB.'sites/default/components/googleConector.js"></script>';
$navHtml .= '<ul class="navNodes">';

for ($iNav = 0; $iNav < $count; $iNav++) {
    $id =  $arrayNav[$iNav]["id"];
    $name = $arrayNav[$iNav]["name"];
    $pathUrl = $arrayNav[$iNav]["pathurl"];
    $target = $arrayNav[$iNav]["target"];
    $level = $arrayNav[$iNav]["level"];
    $level = $arrayNav[$iNav]["level"];
    $path = $arrayNav[$iNav]["path"];

    $navHtml .= '<li class="node node-' . $iNav . '"  pathurl="' . $pathUrl . '" level="' . $level . '" >';
    $navHtml .= '   <a class="nod"  pathurl="' . $pathUrl . '" target="' . $target . '" >' . $name . '</a>';
    if ($arrayNav[$iNav]["cats"] != 0) {
        $navHtml .= '<div class="subMenu">';
        $navHtml .= '   <div class="container">';
        $navHtml .= '       <div class="header"><a item="'. $id.'" pathurl="'.$pathUrl.'" ><h2 class="titleSection">'.$name.'</h2></a></div>';
        $navHtml .= '       <div class="tbody">';
        $arrayCat = $arrayNav[$iNav]["cats"];
        $countCat = count($arrayCat);
        for ($i = 0; $i < $countCat; $i++) {
            $elem = $arrayCat[$i];
            //var_dump($elem);
            $pathurl = $elem["pathurl"];
            $childen = $elem["children"];
            $path = $elem["path"];
            //var_dump($childen);
            
            if ($pathurl == "#"){
                $headerSub = '<label class="label label-'.$i.'" item="'. $elem["id"].'"  pathurl="'.$elem["pathurl"].'">'. $elem["name"].'</label>';
            }else{
                $headerSub = '<div class="header"><a class="nod" item="'. $elem["id"].'" pathurl="'.$elem["pathurl"].'" ><h2 class="titleSection">'.$elem["name"].'</h2></a></div>';
            }

            if ($childen != 0) {
                $navHtml .= '<div class="subMenuElem">';
                $navHtml .= $headerSub;
                $navHtml .= '       <div class="tbody">';
                $arrayChild = $elem["children"];
                $countChild = count($arrayChild);
                for ($j = 0; $j < $countChild; $j++) {
                    $elemChild = $arrayChild[$j];
                    $navHtml .= '<a class="cat cat-' . $j . '" item="' . $elemChild["id"] . '" path="'.$path.'"  pathurl="' . $elemChild["pathurl"] . '" level="' . $elemChild["level"] . '" >' . $elemChild["name"] . '</a>';
                }
                $navHtml .= '       </div>';
                $navHtml .= '</div>';
            }else{
                $navHtml .= '<a class="cat cat-' . $i . '" item="' . $elem["id"] . '" path="'.$path.'"  pathurl="' . $elem["pathurl"] . '" level="' . $elem["level"] . '" >' . $elem["name"] . '</a>';
            }
        }


        $navHtml .= '       </div>';
        $navHtml .= '   </div>';
        $navHtml .= '</div>';
    }
    $navHtml .= '</li>';
}
$navHtml .= '</ul>';


$htmlNav = str_replace("{{_MENUNAV}}", $navHtml, $htmlNav);

//$returnHtml .=  $htmlNav;
echo  $htmlNav;