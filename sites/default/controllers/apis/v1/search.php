<?php
require_once('../../../../../config.php');
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO . "modules/inventory/controllers/class/class.products.php");
$products = new PRODUCTS($fmt);

$ask = $_POST['input'];

if (strlen($ask) < 2) {
    echo json_encode(0);
    exit();
}


$search = "mod_prod_name LIKE '%" . $ask .  "%' OR mod_prod_path_url LIKE '%" . $ask . "%' OR mod_prod_tags LIKE '%" . $ask . "%' OR mod_prod_code LIKE '%" . $ask . "%'";
$sql = "SELECT DISTINCT mod_prod_id, mod_prod_name, mod_prod_path_url,mod_prod_price,mod_prod_img FROM mod_products WHERE mod_prod_state > 1 and $search ORDER BY mod_prod_name asc";

//$sql = "SELECT DISTINCT mod_prod_id, mod_prod_name, mod_prod_path_url, MATCH ( mod_prod_name,mod_prod_path_url,mod_prod_tags,mod_prod_code) AGAINST ('+".$ask."' IN NATURAL LANGUAGE MODE) AS score  FROM mod_products  WHERE mod_prod_state > 0 AND MATCH (mod_prod_name,mod_prod_path_url,mod_prod_tags,mod_prod_code) AGAINST('+".$ask."'' IN NATURAL MODE) HAVING score > 0.2 ORDER BY score ASC LIMIT 0,35";  
$rs = $fmt->querys->consult($sql);
$num = $fmt->querys->num($rs);
if ($num > 0) {
    for ($i = 0; $i < $num; $i++) {
        $row = $fmt->querys->row($rs);
        $idProd = $row["mod_prod_id"];
        // $score = $row["score"];
        // if ($score > 0){
        //     $result[$i] = $row;
        // }
        $result[$i]["id"] = $idProd;
        $result[$i]["name"] = $row["mod_prod_name"];
        $result[$i]["pathUrl"] = $row["mod_prod_path_url"];
        $result[$i]["img"] = _PATH_FILES . $fmt->files->urlAdd($fmt->files->urlFileId($row["mod_prod_img"]), "-thumb");
        $result[$i]["price"] = $products->getOptionsProducts("coin") . $row["mod_prod_price"];
        $result[$i]["priceDiscount"] = $products->getOptionsProducts("coin") . $products->discountProduct($idProd);
    }
    echo json_encode($result);
} else {
    echo json_encode(0);
}

//echo $ask;

/* <div class="box-body container-fluid box-buscar">
    <div class="box-header container">
        <div class="box-title">
            <h2>Resultados de Busqueda "<?php echo $q; ?>"</h2>
        </div>
    </div>
    <div class="container box-body">
    </div>
</div> */
