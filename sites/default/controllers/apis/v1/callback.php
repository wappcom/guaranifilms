<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-Custom-Header");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");


require_once('../../../../../config.php');
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO . "modules/ecommerce/controllers/class/class.carts.php");
$carts = new CARTS($fmt);


switch ($_SERVER["REQUEST_METHOD"]) {
    case 'GET':
        $action = $_GET["action"];
        $code = $_GET["code"];
        $gatewayTransactionId = $_GET["transaction_id"];
        $gatewayInvoiceId = $_GET["invoice_id"];
        $gatewayInvoiceUrl = $_GET["invoice_url"];

        //echo "action:".$action = $_GET["action"]; exit(0);

        $host= $_SERVER["HTTP_HOST"];
        $uri= $_SERVER["REQUEST_URI"];
        $url = "http://" . $host . $uri;  
        $dates = explode("?",$url);
        parse_str(urldecode($dates[1]), $result);
        $json = json_encode($result, JSON_UNESCAPED_UNICODE);


        if ($action == "callback") {
            echo json_encode($carts->updateCallback(array("code" => $code, "gatewayTransactionId" => $gatewayTransactionId, "gatewayJSON" => $json, "gatewayInvoiceUrl" => $gatewayInvoiceUrl)));
            exit(0);
        }
 
        break;  

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        exit(0);
        break;
}