<?php
require_once('../../../../../config.php');
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO . "modules/ecommerce/controllers/class/class.carts.php");
$carts = new CARTS($fmt);

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: JSON");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':

        $bearerToken = $fmt->auth->getInput('bearerToken');
        $entitieId = $fmt->auth->getInput('entitieId');

        if ($fmt->auth->validateBearerToken($bearerToken)) {
            //echo json_encode($entitieId); exit(0);
            echo json_encode($carts->createTempId());
        } else {
            echo $fmt->errors->errorJson([
                "description" => "Error Access Auth BearerToken.",
                "code" => "",
                "lang" => "es"
            ]);
        }

        break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        break;
}
