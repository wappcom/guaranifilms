import { emptyReturn, trimCharacters } from "./index.js"

//Product
export function renderItem(vars) {
    return `
    <div class="item btnProduct" id="btnProducto1">
        <div class="inner">
            <div class="img" style="background:url({{_PATH_FILES}}) no-repeat center center"></div>
            <div class=" text" path-url="{{_PATH_WEB}}productos/709727">
                <div class="precio">Bs 2,50</div>
                <h3 for="">Barbijo Descartable C Clip</h3>
            </div>
            <div class="add ">
                <label for="">Adición rapida al carrito de compras</label>
                <div class="innerAdd">
                    <a class="item btnItemMinus"><i class="icon icon-circle-minus"></i></a>
                    <input type="text" placeholder="0">
                    <a class="item btnItemPlus"><i class="icon icon-circle-plus"></i></a>
                </div>
            </div>
        </div>
    </div>
    `
}

//Loading
export function renderLoading(Cls) {
    return `<div class="loading ${Cls}"></div>`
}

export function renderLoadingLine(Cls) {
    return `<div class="loadingLine ${Cls}"><div class="loadingBarLine"></div></div>`
}

//Search
export function renderReplySearch(vars) {
    const name = vars.name
    const id = vars.id
    const img = vars.img
    const price = vars.price
    const priceDiscount = vars.priceDiscount
    const pathUrl = vars.pathUrl

    let priceTotal = ""

    if (priceDiscount == "" || priceDiscount == "null") {
        priceTotal = "<span>" + price + "</span>"
    } else {
        priceTotal =
            "<span>" + priceDiscount + "</span> Antes <del>" + price + "</del>"
    }

    return `<a class="item item-${id}" id="item${id}" path-url="${pathUrl}">
                <div class="img" style="background:url(${img}) no-repeat center center"></div>
                <div class="text">
                    <label>${name} </label>
                    <div class="price">${priceTotal}</div>
                </div>
            </a>`
}

//SignUp & SignIn & welcome
export function renderSignUp(type) {
    let id = "pageSignUp"
    let btn = ""
    let d = new Date()
    let year = d.getFullYear()
    let yearList = ""
    let yearInit = year - 18
    let yearLimit = yearInit - 105

    let typ = emptyReturn(type, "")

    for (let i = yearInit; i > yearLimit; i--) {
        yearList += `<option value="${i}">${i}</option>`
    }

    //console.log(yearLimit);
    //console.log(yearList);

    return (
        `<div class="pub modalPage pub-signup" id="${id}">
        <div class="inner  mailAccount on animated fadeInUp">
            <div class="head">
                <div class="container">
                    <div class="pullleft">
                        <div class="title">
                            <div class="text">
                                <h2><i class="icon" style="background:url(${_PATH_FILES}sites/default/assets/img/party-popper.png) center center"></i> <span>Crear una cuenta</span></h2>
                                <span>Ingresa los datos para crear tu cuenta con:</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="body form">
                <script src="` +
        _PATH_WEB +
        `sites/default/components/googleConector.js"></script>
                <div class="formControl formSocial">
                    <a class="btn btnSocial btnFb" scope="public_profile,email">
                        <i class="icn icon-facebook"></i> <span lang="es">Facebook</span>
                    </a>
                    <a class="btn btnSocial btnGoogle" id="btnGoogle" data-onsuccess="onSignIn">
                        <i class="icn icon-google"></i> <span lang="es">Google</span>
                    </a>
                </div>
                <div class="formDivider">
                    <span class="line line1" ></span>
                    <span class="text" lang="es">o</span>  
                    <span class="line line2" ></span>
                </div>
                <div class="formGroup formAccount">
                    <div class="formControl formIconAddon formControlEmail">
                        <i class="icon icon-envelope"></i>
                        <input class="formInput" id="inputEmail" name="inputEmail" value="" type="text" placeholder="Ingresa tu E-mail" />
                        <div id="inputEmailMessage"></div>
                    </div>                
                    <!-- <div class="formControl formDialSimple">
                        <div class="dial">
                            <span class="img" style="background: url(${_PATH_WEB}sites/default/assets/img/bo.png) center center;  "></span>
                            <label>+591</label>
                        </div>
                        <input class="formInput" id="inputCelular" name="inputCelular" value="" placeholder="Ingresa tu Celular" type="text" />
                    </div> -->
                    <div class="formControlLandscape formControlNames">
                        <div class="formControl formIconAddon">
                            <i class="icon icon-user"></i>
                            <input class="formInput" id="inputName" name="inputName" value="" type="text" placeholder="Nombre" />
                        </div>                         
                        <div class="formControl">
                            <input class="formInput" id="inputLastname" name="inputLastname" value="" type="text" placeholder="Apellido.s" />
                        </div> 
                    </div> 
                    <div class="formGroup formBirthday">
                        <label>Fecha de Nacimiento</label>
                        <div class="group">
                            <div class="formControl">
                                <label>Día</label>
                                <select class="formInput" id="inputDay" name="inputDay" value="" >
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9" selected>9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </div>
                            <div class="formControl">
                                <label>Mes</label>
                                <select class="formInput" id="inputMonth" name="inputMonth"  >
                                    <option value="1">Ene</option>
                                    <option value="2">Feb</option>
                                    <option value="3">Mar</option>
                                    <option value="4">Abr</option>
                                    <option value="5">May</option>
                                    <option value="6">Jun</option>
                                    <option value="7">Jul</option>
                                    <option value="8" selected>Ago</option>
                                    <option value="9">Sep</option>
                                    <option value="10">Oct</option>
                                    <option value="11">Nov</option>
                                    <option value="12">Dic</option>
                                </select>
                            </div>
                            <div class="formControl">
                                <label>Año</label>
                                <select class="formInput" id="inputYear" name="inputYear"   >
                                    ${yearList}
                                </select>
                            </div>
                        </div>
                    </div> 
                    <div class="formControl formControlNoMargin">
                        <div class="checkboxControl checkboxControlSmall">
                            <input type="checkbox" name="AP" id="AP" checked>
                            <div class="checkboxControlLabel"><span>Acepto el </span><a href="${_PATH_WEB}politicas-de-privacidad">Acuerdo de confidencialidad y terminos de uso del servicio</a></div>
                        </div>
                    </div>
                    <div class="formControl formActions">
                        <a class="btn btnlink btnCancel" for="${id}">Cancelar</a>
                        <a class="btn btnPrimary btnCreateAccount" for="${id}" type="${typ}" id="btnCreateAccount"><span>Crear Cuenta</span></a>
                    </div>
                </div>
        </div>
        <div class="footer">
            Si ya tienes una cuenta <a class="btnSignIn">click aquí</a>
        </div>
        
    </div>
    ` +
        renderSendMailAccount() +
        `
    <div class="bgClosePage" for="${id}" btn="${btn}"></div>
</div>`
    )
}

export const renderActivationAccount = () => {
    return `<div class="pub-activation">
                <div class="boxInner">
                    <form class="form">
                        <label for=""><h2>¡Solo un paso más!</h2></label>
                        <div class="formControl formCheckOk">
                            <label class="noMargin">
                                <b>Añade un Password para activar tu cuenta: </b>
                            </label>
                            <span class="reference">Los password fuertes son mayores a 6 digitos, incluyen números, letras y signos.</span>
                            <input type="password" class="formInput" id="inputPasswordActivation" autofocus placeholder="">
                            <div class="message" id="menssagePassword"></div>
                        </div>
                        <div class="formControl formCheckOk">
                            <label for="">Confirmación de Password:</label>
                            <input type="password" autocomplete="nones" class="formInput" id="inputPasswordConfirm" placeholder="">
                            <div class="message" id="menssagePasswordConfirm"></div>
                        </div>
                        <!--<div class="formControl">
                            <div class="checkboxControl checkboxControlSmall">
                                <input type="checkbox" class="" name="AP" id="AP" checked>
                                <div class="checkboxControlLabel"> Acepto el <a class="{{_PATH_WEB}}"> Acuerdo de confidencialidad y terminos de uso</a> </div>
                            </div>
                        </div>-->
                        <div class="formControl ">
                            <a class="btn btnLg btnCenter btnSuccess btnEnter disabled" action="activateAccount"><span>Activar Cuenta</span></a>
                        </div>
                    </form>
                </div>
            </div>`
}

export const renderSignIn = (type) => {
    let id = "pageSignIn"
    let btn = "btnSignIn"
    let typ = emptyReturn(type, "")
    return (
        `<div class="pub modalPage pub-signin" id="${id}">
    <div class="inner on animated fadeIn">
        <div class="head">
            <div class="container">
                <div class="pullleft">
                    <div class="title">
                        <div class="text">
                            <h2><span>Ingresa a tu cuenta</sspan></h2>
                            <span>Ingresa los datos para ingresar a tu cuenta:</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body form">
                <script src="` +
        _PATH_WEB +
        `sites/default/components/googleConector.js"></script>
                <div class="formControl formSocial">
                    <a class="btn btnSocial btnFb" scope="public_profile,email">
                        <i class="icn icon-facebook"></i> <span lang="es">Facebook</span>
                    </a>
                    <a class="btn btnSocial btnGoogle" id="btnGoogle" data-onsuccess="onSignIn">
                        <i class="icn icon-google"></i> <span lang="es">Google</span>
                    </a>
                </div>
                <div class="formDivider">
                    <span class="line line1" ></span>
                    <span class="text" lang="es">o</span>  
                    <span class="line line2" ></span>
                </div>
                <div class="formGroup formAccount">
                    <div class="formControl formIconAddon formControlEmail">
                        <i class="icon icon-envelope"></i>
                        <input class="formInput" id="inputEmail" name="inputEmail" value="" type="text" placeholder="Ingresa tu E-mail" />
                    </div>                
                    
                    <div class="formControl formIconAddon formControlEmail">
                        <i class="icon icon-lock"></i>
                        <a class="btnBlockPw" state="0" for="inputPassword"><i class="icon icon-eye-close"></i></a>
                        <input class="formInput" id="inputPassword" name="inputPassword" value="" type="password" placeholder="Password" />
                    </div> 
                    <div class="formControl formControlNoMargin">
                        <div class="messageSignIn"></div>
                        <a class="btnLinkUnderline btnForgotPassword"><span>¿Olvidaste tu contraseña?</span></a>
                    </div>
                    <div class="formControl formActions">
                        <a class="btn btnlink  btnCancel" for="${id}"  btn="${btn}" >Cancelar</a>
                        <a class="btn btnSuccess  btnLogin"  type="${typ}" for="${id}" id="btnLogin"><span>Ingresar</span></a>
                    </div>
                </div>
        </div>
        <div class="footer">
            Si no te has registrado todavia haz <a class="btnSignUp">click aquí</a>
        </div>
    </div>
    <div class="bgClosePage" for="${id}" btn="${btn}"></div>
</div>`
    )
}

export const renderExistAccount = () => {
    return `Si es tu email puedes: <a class="btnSignIn"><b>Ingresar a la cuenta</b></a>`
}

export const renderSendMailAccount = () => {
    return `<div class="inner sendMailAccount">
        <div class="head">
            <h2>
                <img src="${_PATH_WEB}sites/default/assets/img/send.svg" alt=""> 
                <span>¡Activemos tu cuenta!</span>
            </h2>
            <span>
                Enviamos el CODIGO de activación a tu email, <b>por favor revisa tu bandeja</b>. 
            </span>
        </div> 
        <div class="tbody form">
            <form id="formCode">
                <input class="inputCode" n="0" maxlength="6" id="inputCode0" type="text" autofocus />
                <input class="inputCode" n="1" maxlength="1" id="inputCode1" type="text"  />
                <input class="inputCode" n="2" maxlength="1" id="inputCode2" type="text"  />
                <input class="inputCode" n="3" maxlength="1" id="inputCode3" type="text"  />
                <input class="inputCode" n="4" maxlength="1" id="inputCode4" type="text"  />
                <input class="inputCode" n="5" maxlength="1" id="inputCode5" type="text"  />
            </form>
            <div class="recode">¿No te llego el código? <a class="btnResend" mail="" >Reenviar Código</a></div>
            <div class="btns">
                <a class="btn btnPrimary btnCenter btnCheckCode disabled" id="btnCheckCode"><span>Verificar Código</span></a>
            </div>
            <div class="foot">Si aún asi no te llega el código <a href="mailto:contacto@farmaline.com.bo">contacto@farmaline.com.bo</a> </div>
        </div>
        
   </div>`
}

export const renderWelcome = (str) => {
    let label = ""
    if (str != "" || str == undefined) {
        label = str
    }
    return ` <div class="welcome animated bounce infinite"> 
                <img src="${_PATH_WEB}sites/default/assets/img/ok.svg" >
                <h2>Bienvenido ${label} </2>
            </div>`
}

//Cart
export const renderItemCart = (vars) => {
    const item = vars.item
    const concept = vars.concept
    const pathImg = vars.pathImg
    const quantity = vars.quantity
    const price = vars.price

    return `<div class="itemProductCart" item="${item}" id="itemProductCart${item}">
                <div class="details">
                    <div class="img" style="background: url(${
                        _PATH_FILES + pathImg
                    })center center"></div>
                    <div class="concept">${concept}</div>
                </div>
                <div class="actionsCount">
                    <div class="price">${price}</div>
                    <div class="innerAdd">
                        <a class="item btnItemMinus" typebtn="btnCart" item="${item}"><i class="icon icon-circle-minus "></i></a>
                        <input type="text " name="inputProductCart[]" item="${item}" id="inputProductCart${item}" value="${quantity}" />
                        <a class="item btnItemPlus" typebtn="btnCart" item="${item}"><i class="icon icon-circle-plus "></i></a>
                        <a class="item btnItemDeleteCart" item="${item}"><i class="icon icon-trash "></i></a>
                    </div>
                </div>
            </div>`
}

export const renderItemCartPurchase = (vars) => {
    const item = vars.item
    const concept = vars.concept
    const pathImg = vars.pathImg
    const quantity = vars.quantity
    const price = vars.price

    return `<div class="itemProductCart " item="${item}" id="itemProductCart${item}">
                <div class="details">
                    <div class="img" style="background: url(${
                        _PATH_FILES + pathImg
                    })center center"></div>
                    <div class="concept">${concept}</div>
                </div>
                <div class="actionsCount">
                    <div class="price">${price}</div>
                </div>
            </div>`
}

export const renderMessageNoItem = () => {
    return '<div class="comment commentWarning">No hay Items comprados <a class="btnLinkUnderline btnLinkHome">Seguir Comprando</a></div>'
}

export const renderMessageCreateAccount = () => {
    return '*  <a class="btnSignUp" type="fromCart"><b>Crear una cuenta</b></a>  o  <a class="btnSignIn" type="fromCart"><b>Ingresar a una cuenta</b></a> para la Entrega'
}

export const renderMessageAccountExist = (name = "") => {
    return `Cuenta de: <b>${name}</b>`
}

export const renderMessageCreateAddress = () => {
    return '*  <a class="btnAddAddresses disabled" type="fromCart"> Añade una dirección para la Entrega</a>'
}

export const renderMessageCreateInvoice = () => {
    return `<div class="formControlLandscape formControlInvoice">
                        <div class="formControl form40w">
                            <input class="formInput" id="inputRazonSocial" name="inputRazonSocial" value="" type="text" placeholder="Razon Social" />
                        </div>                         
                        <div class="formControl form25w">
                            <input class="formInput" id="inputNit" name="inputNit" value="" type="text" placeholder="NIT" />
                        </div> 
                    </div> `
}

export const renderMessageCreatePay = () => {
    return (
        `<div class="formControl formControlPay">
                <div class="checkboxControl">
                    <input type="radio" class="formCheckInput" name="optionsPay" id="optionsPay1" value="multipay" checked>
                    <label> Multipago <img src="` +
        _PATH_WEB +
        `sites/default/assets/img/methodpay.jpg" ></label>
                </div>                
                <div class="checkboxControl">
                    <input type="radio" class="formCheckInput" name="optionsPay" id="optionsPay2" value="cash-on-delivery" >
                    <label> Pago contra Entrega  </label>
                </div>
            </div> `
    )
}

//Address
export const renderAddresses = (type) => {
    let id = "modalAddresses"
    let btn = ""
    let keyGoogle = "AIzaSyAFo0fR9u9SJvgCfVhHPXWbdib2Vm-4l8s"
    let typ = emptyReturn(type, "")
    let poly = ""

    for (let i = 0; i < perimetersDates.length; i++) {
        const elem = perimetersDates[i]
        const id = elem.id
        const name = elem.name
        const code = elem.code
        const perimetrer = elem.perimetrer
        const cost = elem.cost

        poly += renderPerimetrer({
            id: id,
            name: name,
            perimetrer: perimetrer,
            cost: cost,
            code: code,
        })
    }

    return /*html*/ `<div class="pub modalPage pub-${id}" id="${id}">
                <script>
                    var myPositionNow = ""
                    var maxGeo = 1;

                    function initMap() {

                        var lat = -17.786674;
                        var long = -63.180237;

                        var styleArray = [{
                                "featureType": "administrative",
                                "elementType": "geometry",
                                "stylers": [{
                                    "visibility": "off"
                                }]
                            },
                            {
                                "featureType": "poi",
                                "stylers": [{
                                    "visibility": "off"
                                }]
                            },
                            {
                                "featureType": "road",
                                "elementType": "labels.icon",
                                "stylers": [{
                                    "visibility": "off"
                                }]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry",
                                "stylers": [{
                                    "visibility": "simplified"
                                }]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#ffffff"
                                }]
                            },
                            {
                                "featureType": "transit",
                                "stylers": [{
                                    "visibility": "off"
                                }]
                            }
                        ];

                        var mapOptions = {
                            zoom: 16,
                            styles: styleArray,
                            center: new google.maps.LatLng(lat, long),
                            gestureHandling: 'greedy',
                            streetViewControl: false,
                            disableDefaultUI: true,
                            zoomControlOptions: {
                                position: google.maps.ControlPosition.RIGHT_BOTTOM
                            }
                        };
                        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
                        var infoWindow = new google.maps.InfoWindow({
                            content: '',
                            buttons: {
                                close: { visible: false }
                            }
                        })

                        $('<div id="windowInfo"/>').addClass('centerMarker').appendTo(map.getDiv()).click(function() {
                            if (formattedAddress) {
                                infoWindow.bindTo('position', map, 'center');
                                infoWindow.open(map);
                            }
                        })

                        var geocoder = new google.maps.Geocoder()
                        var formattedAddress = null

                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                                var pos = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude
                                }
                                myPositionNow = position.coords.latitude + "," + position.coords.longitude
                                console.log(myPositionNow)

                                geocodeLatLng(position.coords.latitude, position.coords.longitude, geocoder, map)
                                map.setCenter(pos)

                            }, function() {
                                console.log("no geolocation");
                                handleLocationError(true, map.getCenter());
                            })
                        } else {
                            // Browser doesn't support Geolocation
                            console.log("browser doesn't support Geolocation");
                            handleLocationError(false, map.getCenter());
                        }



                        function geocodeLatLng(lat, lng, geocoder, map) {
                            const latlng = {
                                lat: parseFloat(lat),
                                lng: parseFloat(lng),
                            };
                            geocoder.geocode({ location: latlng }, (results, status) => {
                                if (status === "OK") {
                                    if (results[0]) {
                                        $("#inputCoord").val(lat + "," + lng)
                                            //console.log(results[0].formatted_address);
                                        $("#inputAddressLabel").val(results[0].formatted_address)
                                        $(".btnClearInputSearchAddress").addClass("on")
                                        //console.log(perimetersDates)
                                        
                                        var myPosition = new google.maps.LatLng(lat, lng);
                                        $("#inputPerimeter").val("0");
                                        $("#inputDeliveryCost").val("0");
                                        ${poly}
                                    } else {
                                        window.alert("No results found");
                                    }
                                } else {
                                    window.alert("Geocoder failed due to: " + status);
                                }
                            });
                        }
                        //updateAddress(lat, long)

                        function gEventCallback(e) {
                            geocodeLatLng(this.getCenter().lat(), this.getCenter().lng(), geocoder, map);
                        }

                        function SearchgeocodeAddress(geocoder, resultsMap) {
                            const address = document.getElementById("address").value;
                            geocoder.geocode({ address: address }, (results, status) => {
                                if (status === "OK") {
                                    resultsMap.setCenter(results[0].geometry.location);
                                    new google.maps.Marker({
                                        map: resultsMap,
                                        position: results[0].geometry.location,
                                    });
                                } else {
                                    alert("Geocode was not successful for the following reason: " + status);
                                }
                            });
                        }

                        function handleLocationError(browserHasGeolocation, pos) {
                            console.log("no geolocation");
                            console.log(browserHasGeolocation ?
                                'Error: The Geolocation service failed.' :
                                'Error: Your browser doesnt support geolocation.');
                        }

                        function geocodeAddress(geocoder, resultsMap) {
                            let address = document.getElementById("inputAddressLabel").value;
                            address += ", Santa Cruz de la Sierra, Bolivia" 
                            geocoder
                                .geocode({ address: address })
                                .then(({ results }) => {
                                resultsMap.setCenter(results[0].geometry.location);
                                map.setZoom(18);
                                /* new google.maps.Marker({
                                    map: resultsMap,
                                    position: results[0].geometry.location,
                                }); */
                                })
                                .catch((e) =>
                                    alert("Geocode was not successful for the following reason: " + e)
                                );
                        }

                        
                        $("body").on("click", ".btnSearchAddress", function(e) {
                            e.preventDefault()
                            e.stopPropagation()
                            e.stopImmediatePropagation()
                            const inputAddress = $("#inputAddressLabel").val()

                            if (inputAddress != "" || inputAddress != undefined) {
                                $(".btnClearInputSearchAddress").addClass("on")
                                geocodeAddress(geocoder, map);
                            }
                        })

                        $("body").on("change", "#inputAddressLabel", function(e) {
                            e.preventDefault()
                            e.stopPropagation()
                            e.stopImmediatePropagation()
                            $(".btnClearInputSearchAddress").addClass("on")
                            geocodeAddress(geocoder, map);
                        })

                        //google.maps.event.addListener(map, 'drag', gEventCallback);
                        google.maps.event.addListener(map, 'dragend', gEventCallback);
                        //google.maps.event.addListener(map, 'zoom_changed', gEventCallback);
                    }
                </script>                
                <div class="inner on">
                    <div class="listAddresses on">
                        <div class="head">
                            <label><i class="btnCancel icon  icon-arrow-line-left" for="${id}"></i><h2>Mis direcciones</h2></label>
                            <div class="boxActions">
                                <a class="btn btnLink btnIconRight btnAddAddress"><span>Agregar una dirección</span><i class="icon  icon-circle-plus"></i></a>
                            </div>
                        </div>
                        <div class="list listAddressAccount"></div> 
                    </div>
                    <div class="mapAddress">
                        <div class="head">
                            <a class="btnCloseMap"><i class="icon  icon-arrow-line-left"></i></a>
                            <div class="messageMap"></div>
                        </div>
                        <div class="boxSearchAddress">
                            <a class="btnListExp"></a>
                            <label><h3>¿A donde enviaremos tus pedidos?</h3><span>Puedes nombrar a gusto esta dirección.</span></label>
                            <input type="hidden" id="inputCoord" >
                            <input type="hidden" id="inputPerimeter" >
                            <input type="hidden" id="inputDeliveryCost" >
                            <div class="searchAddress">
                                <div class="boxSearch">
                                <a class="btnClearInputSearchAddress"><i class="icon icon-circle-close"></i></a>
                                <a class="btnSearchAddress"><i class="icon icon-search"></i></a>
                                <input class="inputAddressLabel" id="inputAddressLabel" type="input" placeholder="Buscar Dirección" >
                                </div>
                                <a class="btn btnSuccess btnAddAddressPlace" id="btnAddAddressPlace">Agregar</a>
                            </div>
                        </div>
                        <div id="map"></div>
                    </div>
                    <script async defer src="https://maps.googleapis.com/maps/api/js?key=${keyGoogle}&libraries=geometry&callback=initMap"></script>
                </div>
            <div class="bgClosePage" for="${id}" btn="${btn}"></div>
    </div>`
}

export const renderPerimetrer = (vars) => {
    return `var perimetro${vars.id} = new google.maps.Polyline({
                path: [
                    ${vars.perimetrer}
                ]
            }); 
            perimetro${vars.id}.setMap(map);
            if (google.maps.geometry.poly.containsLocation(myPosition,perimetro${vars.id},0.00001)) {
                $("#inputPerimeter").val("${vars.id}");
                $("#inputDeliveryCost").val("${vars.cost}");
            } 
            `
}

export const renderPerimetrerMessageNotCovered = () => {
    return `<div class="alert alertDanger">Lo sentimos, todavía no tenemos covertura en esta dirección.</div>`
}

export const renderAddressToList = (vars) => {
    const addId = emptyReturn(vars.addId, "")
    const address = trimCharacters(emptyReturn(vars.address, ""), 35, "...")
    const perimeter = emptyReturn(vars.perimeter, "")
    const deliveryCost = emptyReturn(vars.deliveryCost, "")
    const order = emptyReturn(vars.order, "")
    let principal = ""

    if (order == 0) {
        principal = '<span class="principal">principal</span>'
    } else {
        principal = ``
    }

    return `<div class="address address-${addId} animated backInRight animated" item="${addId}" >
                <div class="title">
                    <i class="icon icon-pointer"></i>
                    <span>${address}</span>
                </div>
                <div class="actions on" item="${addId}">
                    ${principal}
                    <a class="btnAddressPrincipal" item="${addId}" perimeter="${perimeter}"><i class="icon icon-checked"></i></a>
                    <buttom class="btnAddressDelete"  item="${addId}"><i class="icon icon-trash"></i></buttom>
                    <buttom class="btnAddressActive" address="${vars.address}" item="${addId}" deliverycost="${deliveryCost}" perimeter="${perimeter}"><i class="icon icon-chevron-right"></i></buttom>
                </div>
                <div class="removeAddress" item="${addId}" >
                    <button class="btnRemoveAddress btn btnMini btnDanger " id="btnRemoveAddress" item="${addId}">
                        <i class="icon icon-trash"></i>
                        <span>¿Estas seguro?</span>
                    </button>
                    <button class="btnCancelAddress btn btnMini btnFull " item="${addId}">Cancelar</button>
                </div>
            </div>`
}

export const renderAddressCart = (vars) => {
    const address = trimCharacters(emptyReturn(vars.address, ""), 35, "...")
    return `
        <span class="address">${address}</span>
        <input type="hidden" name="inputPerimeterAddress" id="inputPerimeterAddress" value="${vars.perimeter}" >
        <input type="hidden" name="inputAddressId" id="inputAddressId" value="${vars.item}" >
        <input type="hidden" name="inputDeliveryCost" id="inputDeliveryCost" value="${vars.deliveryCost}" >
        <input type="hidden" name="inputAddressName" id="inputAddressName" value="${vars.address}" >
        <a class="btnChangeAddresss btnAddAddresses"><span lang="es">Cambiar dirección</span></a>
   `
}

export const renderBirthDate = (data) => {
    let d = new Date()
    let year = d.getFullYear()
    let yearList = ""
    let yearInit = year - 18
    let yearLimit = yearInit - 105

    for (let i = yearInit; i > yearLimit; i--) {
        yearList += `<option value="${i}">${i}</option>`
    }

    return `<div class="head">
                <div class="container">
                    <div class="pullleft">
                        <div class="title">
                            <div class="text">
                                <h2><i class="icon" style="background:url(https://farmaline.com.bo/sites/default/assets/img/party-popper.png) center center"></i> <span>Activar cuenta</span></h2>
                                <span>Solo necesitamos un dato más para darle de alta a tu cuenta:</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="body form">
                <script src="https://farmaline.com.bo/sites/default/components/googleConector.js"></script>
                
                <div class="formGroup formAccount">                     
                    <div class="formGroup formBirthday">
                        <label>Fecha de Nacimiento</label>
                        <div class="group">
                            <div class="formControl">
                                <label>Día</label>
                                <select class="formInput" id="inputDay" name="inputDay" value="">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9" selected="">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </div>
                            <div class="formControl">
                                <label>Mes</label>
                                <select class="formInput" id="inputMonth" name="inputMonth">
                                    <option value="1">Ene</option>
                                    <option value="2">Feb</option>
                                    <option value="3">Mar</option>
                                    <option value="4">Abr</option>
                                    <option value="5">May</option>
                                    <option value="6">Jun</option>
                                    <option value="7">Jul</option>
                                    <option value="8" selected="">Ago</option>
                                    <option value="9">Sep</option>
                                    <option value="10">Oct</option>
                                    <option value="11">Nov</option>
                                    <option value="12">Dic</option>
                                </select>
                            </div>
                            <div class="formControl">
                                <label>Año</label>
                                <select class="formInput" id="inputYear" name="inputYear">
                                    ${yearList}
                                </select>
                            </div>
                        </div>
                    </div> 
                    <div class="formControl formControlNoMargin">
                        <div class="checkboxControl checkboxControlSmall">
                            <input type="checkbox" name="AP" id="AP" checked="">
                            <div class="checkboxControlLabel"><span>Acepto el </span><a href="https://farmaline.com.bo/politicas-de-privacidad">Acuerdo de confidencialidad y terminos de uso del servicio</a></div>
                        </div>
                    </div>
                    <div class="formControl formActions">
                        <a class="btn btnlink btnCancel" for="pageSignUp" data-accesstoken="${data.access_token}" data-refreshtoken="${data.refresh_token}">Cancelar</a>
                        <a class="btn btnPrimary" for="pageSignUp" type="" id="btnConfigByrthdate" data-accesstoken="${data.access_token}" data-refreshtoken="${data.refresh_token}"><span>Activar Cuenta</span></a>
                    </div>
                </div>
        </div>`
}
