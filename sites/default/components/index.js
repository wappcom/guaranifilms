import {
    renderLoading,
    renderLoadingLine
} from "./render.js"

import {
    removeMenus
} from "./nav.js"

localStorage.setItem("bearerToken", _BEARER_TOKEN)
localStorage.setItem("clientId", _CLIENT_ID)
localStorage.setItem("clientSecret", _CLIENT_SECRET)
localStorage.setItem("entitieId", "1")
localStorage.setItem("ga_client_id", "")
localStorage.setItem("ga_state", "")

var zoneTime = Intl.DateTimeFormat().resolvedOptions()
var pageNow = ""
var pageHome = ""
let porc = 0
const urlNow = window.location.href
const urlNowStr = urlNow.replace(_PATH_WEB, "")

document.addEventListener("DOMContentLoaded", function () {
    //loadPageStatus(urlNowStr)
    //console.log("DOMContentLoaded");
    /*  loadView(_PATH_WEB + "sites/default/controllers/tpl/nav.tpl.php?" + _VS).then((response) => {
         //console.log('loadView', response);
         let nav = $("nav");
         console.log(nav.length);
         if (nav.length == 0) {
             $("#bodyIndex").prepend(response);
         }
     }).catch(console.warn()); */
})

export const loadLoading = (str) => {
    $(str).prepend(renderLoading(""))
}

export const removeLoading = () => {
    $(".loading").remove()
}

export const removeModalPage = (type) => {
    if (type == undefined) {
        $(".modalPage").remove()
    }

    if (type == "animated") {
        $(".modalPage").addClass("animated fadeOut")
        setInterval(() => {
            $(".modalPage").remove()
        }, 100)
    }
}

export const trimCharacters = (str = "", length = "", end = "") => {
    return str.substring(0, length) + end
}

export const getUrlLocation = (vars) => {
    //console.log('getUrlLocation');
    let url = window.location.href;
    let urlStr = url.replace(_PATH_WEB, "");
    console.log('getUrlLocation', url, _PATH_WEB, urlStr);
    return urlStr;

}

export const getScript = (path, tag) => {
    var newNode = document.createElement("script") // create a script tag
    referenceNode = document.querySelector(tag)
    newNode.src = path // set the source of the script to your script
    referenceNode.append(newNode) // append the script to the DOM
    //console.log('scritpCargado:' + path)
}

export const isJson = (str) => {
    //GET
    try {
        JSON.parse(str)
    } catch (e) {
        return false
    }
    return true
}

export const replacePath = (str) => {
    //console.log("replacePath " + str)
    str = str.replace(/{{_PATH_WEB}}/g, _PATH_WEB)
    str = str.replace(/{{_PATH_WEB_NUCLEO}}/g, _PATH_WEB_NUCLEO)
    str = str.replace(/{{_PATH_FILES}}/g, _PATH_FILES)
    str = str.replace(/{{_PATH_DASHBOARD}}/g, _PATH_DASHBOARD)
    return str
}

export const outId = (id) => {
    $("#" + id).addClass("animated fadeOut")
    setTimeout(() => {
        $("#" + id).remove()
    }, 220)
}

export const outElem = (elm) => {
    $(elm).addClass("animated fadeOut")
    setTimeout(() => {
        $(elm).remove()
    }, 220)
}

export const disabledElem = (elm) => {
    if (elm != "") {
        $(elm).addClass("disabled")
    }
}

export const unDisabledElem = (elm) => {
    if (elm != "" || elm != ".") {
        $(elm).removeClass("disabled")
    }
}

export const loadLoadingLine = (str) => {
    porc = 0
    $(str).prepend(renderLoadingLine(""))
    let displayLoadingLine = setInterval(loadBarLine, 200)
    //console.log(displayLoadingBar)
}

export const loadBarLine = () => {
    porc = porc + 1
    //console.log(porc)
    $(".loadingBarLine").width(porc + "%")
    if (porc > 99) {
        porc = 0
    }
}

export const removeLoadingLine = () => {
    $(".loadingLine").remove()
}

export const loadBtnLoading = (e) => {
    //console.log(e)
    let id = e.currentTarget.id
    let dat = e.currentTarget.innerHTML
    $("#" + id).addClass("btnIconLeft disabled")
    e.currentTarget.innerHTML =
        '<i class="icon icon-refresh icon-loading"></i>' + dat
}

export const removeBtnLoading = (e) => {
    //console.log(e)
    let id = e.currentTarget.id
    let dat = e.currentTarget.innerHTML
    $("#" + id).removeClass("btnIconLeft")
    let html = dat.replace('<i class="icon icon-refresh icon-loading"></i>', "")
    e.currentTarget.innerHTML = html
}

export const replaceAll = function (text, search, replace) {
    while (text.toString().indexOf(search) != -1)
        text = text.toString().replace(search, replace)
    return text
}

export const loadView = async (url) => {
    let req = new Request(url, {
        method: "POST",
        mode: "cors",
        async: true,
    })

    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.text()
        return str
    } catch (err) {
        console.log("loadView Error:" + err)
    }
}

export const pageActive = (page) => {
    $(".bodyPage >.block").removeClass("active")
    $(".bodyPage " + page).addClass("active")
    localStorage.setItem("pageActive", page)
}

export const dayDate = () => {
    let d = new Date()
    return (
        d.getFullYear() +
        "-" +
        (d.getMonth() + 1) +
        "-" +
        d.getDate() +
        "," +
        d.getHours() +
        ":" +
        d.getMinutes() +
        ":" +
        d.getSeconds()
    )
}

export const validateEmail = (valor) => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(valor)) {
        //alert("La dirección de email " + valor + " es correcta.");
        return 1
    } else {
        //alert("La dirección de email es incorrecta.");
        return 0
    }
}

export const loadBtnLoadingElem = (elm) => {
    //console.log(e)
    let dat = $(elm).html()
    $(elm).addClass("btnIconLeft disabled")
    $(elm).html('<i class="icon icon-refresh icon-loading"></i>' + dat)
}

export const loadingBtnIcon = (elm) => {
    $(elm).addClass("btnIconLeft disabled")
    $(elm + " i").removeClass()
    $(elm + " i").addClass("icon icon-refresh icon-loading")
}

export const accessToken = {
    access_token: localStorage.getItem("access_token"),
    refresh_token: localStorage.getItem("refresh_token"),
    entitieId: localStorage.getItem("idEntitie"),
    page: _PATH_PAGE,
}

export const removeLoadingIcon = (elm) => {
    $(elm).removeClass("btnIconLeft")
    $(elm).removeClass("disabled")
    $(elm + " i").removeClass()
}

export const removeBtnLoadingElem = (elm) => {
    console.log(elm)
    let dat = $(elm).html()
    $(elm).removeClass("btnIconLeft")
    let html = dat.replace('<i class="icon icon-refresh icon-loading"></i>', "")
    $(elm).html(html)
}

export const emptyReturn = (str, ret) => {
    //console.log(str, ret)
    if (str == "" || str == undefined) {
        return ret
    } else {
        return str
    }
}

export const changeBtnLabel = (e, str) => {
    let id = e.currentTarget.id

    $("#" + id + " span").html(str)
}

export const changeBtnLabelElem = (elem, str) => {
    $(elem + " span").html(str)
}

export const forcePass = (str, elem) => {
    var strongRegex = new RegExp(
        "^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$",
        "g"
    )
    var mediumRegex = new RegExp(
        "^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$",
        "g"
    )
    var enoughRegex = new RegExp("(?=.{6,}).*", "g")

    if (false == enoughRegex.test(str)) {
        $(elem).addClass("error")
        $(elem).html("<span>Adicion + caracteres.</span>")
        return false
    } else if (strongRegex.test(str)) {
        // $(elem).addClass('heavy');
        // $(elem).html('<span>El Password es Fuerte!</span>');
        return true
    } else if (mediumRegex.test(str)) {
        $(elem).addClass("medium")
        $(elem).html("<span>El password tiene fuerza media!</span>")
        return true
    } else {
        $(elem).addClass("error")
        $(elem).html("<span>El password es muy Débil!</span>")
        return false
    }
    // console.log('id:'+ elem);
    //return true;
}

export const alertPage = (array) => {
    //console.info(array);
    var tipe = array.tipe
    var text = array.text
    var icon = array.icon
    var num = empty(array.num, 1)
    var stop = array.stop
    var animation_in = array.animation_in
    var animation_out = array.animation_out
    var time = array.time
    var position = array.position
    var contAlert = 0
    $(".boxAlert").each(function () {
        contAlert++
    })
    if (contAlert == 0) {
        $("body").append('<div class="boxAlert ' + position + '"></div>')
    }
    $("body .boxAlert").append(
        "<div num='" +
        num +
        "' class='m-alert m-alert-num-" +
        num +
        " m-alert-" +
        tipe +
        " animated " +
        animation_in +
        " '><i class='icon " +
        icon +
        "'></i><span class='text'>" +
        text +
        "</span></div>"
    )
    $(".alert").removeClass(animation_in)

    if (stop != 1) {
        setTimeout(function () {
            //console.log(time);
            $(".m-alert").addClass(animation_out)
            setTimeout(function () {
                $(".boxAlert").html("")
                $(".boxAlert").remove()
            }, 450)
        }, time)
    }
}

export const empty = (vars) => {
    if (vars == undefined || vars == "") {
        return ""
    } else {
        return vars
    }
}