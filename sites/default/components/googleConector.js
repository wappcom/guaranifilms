var googleUser = {}
var startApp = function () {
    gapi.load("auth2", function () {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        auth2 = gapi.auth2.init({
            client_id:
                "223172067837-hb1aibiknmb24pi20oilla4rsr3u7ehq.apps.googleusercontent.com",
            cookiepolicy: "single_host_origin",
            // Request scopes in addition to 'profile' and 'email'
            //scope: 'additional_scope'
        })

        attachSignin(document.getElementById("btnGoogle"))
    })
}

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile()
    // console.log('ID: ' + profile.getId());
    // console.log('Full Name: ' + profile.getName());
    // console.log('Given Name: ' + profile.getGivenName());
    // console.log('Family Name: ' + profile.getFamilyName());
    // console.log('Image URL: ' + profile.getImageUrl());
    // console.log('Email: ' + profile.getEmail());
    console.log(profile)
    var datosArray = new Array()
    datosArray["id"] = profile.getId()
    datosArray["name"] = profile.getGivenName()
    datosArray["lastname"] = profile.getFamilyName()
    datosArray["img"] = profile.getImageUrl()
    datosArray["email"] = profile.getEmail()
    datosArray["token"] = googleUser.getAuthResponse().id_token
    datosArray["type"] = "google" //1. local 2.fb 3. gl */

    createAccountUser(datosArray)
}

function attachSignin(element) {
    //console.log(element.id);
    auth2.attachClickHandler(
        element,
        {},
        function (googleUser) {
            onSignIn(googleUser)
        },
        function (error) {
            //console.log(JSON.stringify(error, undefined, 2));
        }
    )
}

startApp()
