var myPositionNow = ""
var maxGeo = 1

export function initMap() {
    var lat = -17.786674
    var long = -63.180237

    var styleArray = [
        {
            featureType: "administrative",
            elementType: "geometry",
            stylers: [
                {
                    visibility: "off",
                },
            ],
        },
        {
            featureType: "poi",
            stylers: [
                {
                    visibility: "off",
                },
            ],
        },
        {
            featureType: "road",
            elementType: "labels.icon",
            stylers: [
                {
                    visibility: "off",
                },
            ],
        },
        {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [
                {
                    visibility: "simplified",
                },
            ],
        },
        {
            featureType: "road.highway",
            elementType: "geometry.fill",
            stylers: [
                {
                    color: "#ffffff",
                },
            ],
        },
        {
            featureType: "transit",
            stylers: [
                {
                    visibility: "off",
                },
            ],
        },
    ]

    var mapOptions = {
        zoom: 16,
        styles: styleArray,
        center: new google.maps.LatLng(lat, long),
        gestureHandling: "greedy",
        streetViewControl: false,
        disableDefaultUI: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM,
        },
    }
    var map = new google.maps.Map(document.getElementById("map"), mapOptions)
    var infoWindow = new google.maps.InfoWindow({
        content: "",
        buttons: {
            close: { visible: false },
        },
    })

    $('<div id="windowInfo"/>')
        .addClass("centerMarker")
        .appendTo(map.getDiv())
        .click(function () {
            if (formattedAddress) {
                infoWindow.bindTo("position", map, "center")
                infoWindow.open(map)
            }
        })

    var mps = [
        "MULTIPOLYGON((4 47,19.1 50.1,18.1 60,4 47),(3.9 46.9,28.5 46.5,5 30,3.9 46.9))",
        "MULTIPOLYGON((50 57,20.1 47.1,1 1,50 57),(11.9 46.9,31.5 46.5,50 1,11.9 46.9))",
    ]
    for (i in mps) {
        var lines = drawPoly(mps[i].replace("MULTIPOLYGON", ""))
        for (k = 0; k < lines.length; k++) {
            lines[k].setMap(map)
        }
        lines.length = 0
    }

    var geocoder = new google.maps.Geocoder()
    var formattedAddress = null

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                }
                myPositionNow =
                    position.coords.latitude + "," + position.coords.longitude
                console.log(myPositionNow)

                geocodeLatLng(
                    position.coords.latitude,
                    position.coords.longitude,
                    geocoder,
                    map
                )
                map.setCenter(pos)
            },
            function () {
                console.log("no geolocation")
                handleLocationError(true, map.getCenter())
            }
        )
    } else {
        // Browser doesn't support Geolocation
        console.log("browser doesn't support Geolocation")
        handleLocationError(false, map.getCenter())
    }

    function geocodeLatLng(lat, lng, geocoder, map) {
        const latlng = {
            lat: parseFloat(lat),
            lng: parseFloat(lng),
        }
        geocoder.geocode({ location: latlng }, (results, status) => {
            if (status === "OK") {
                if (results[0]) {
                    $("#inputCoord").val(lat + "," + lng)
                    $("#inputCoord").val(lat + "," + lng)
                    //console.log(results[0].formatted_address);
                    $("#inputAddressLabel").val(results[0].formatted_address)

                    console.log(perimetersDates)

                    var myPosition = new google.maps.LatLng(lat, lng)

                    //x
                    //y

                    if (
                        google.maps.geometry.poly.containsLocation(
                            myPosition,
                            lines,
                            0.00001
                        )
                    ) {
                        console.log(lines)
                    }
                } else {
                    window.alert("No results found")
                }
            } else {
                window.alert("Geocoder failed due to: " + status)
            }
        })
    }
    //updateAddress(lat, long)

    function gEventCallback(e) {
        geocodeLatLng(
            this.getCenter().lat(),
            this.getCenter().lng(),
            geocoder,
            map
        )
    }

    function SearchgeocodeAddress(geocoder, resultsMap) {
        const address = document.getElementById("address").value
        geocoder.geocode({ address: address }, (results, status) => {
            if (status === "OK") {
                resultsMap.setCenter(results[0].geometry.location)
                new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                })
            } else {
                alert(
                    "Geocode was not successful for the following reason: " +
                        status
                )
            }
        })
    }

    function handleLocationError(browserHasGeolocation, pos) {
        console.log("no geolocation")
        console.log(
            browserHasGeolocation
                ? "Error: The Geolocation service failed."
                : "Error: Your browser doesn't support geolocation."
        )
    }

    //google.maps.event.addListener(map, 'drag', gEventCallback);
    google.maps.event.addListener(map, "dragend", gEventCallback)
    //google.maps.event.addListener(map, 'zoom_changed', gEventCallback);
}
