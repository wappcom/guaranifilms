document.addEventListener("DOMContentLoaded", function() {});

var token = "";
var idiomaApp = "es_ES";


window.fbAsyncInit = function() {
    FB.init({
        appId: '507558700697659',
        cookie: true,
        status: true,
        xfbml: true,
        version: 'v10.0'
    });

    FB.AppEvents.logPageView();


    $("body").on('click', '.btnFb', function(event) {
        event.preventDefault();
        event.stopPropagation();
        /* Act on the event */
        //console.log("prueba api");
        //testAPI();
        //checkLoginState();

        FB.login(function(response) {
            if (response.status === 'connected') {

                token = response.authResponse.accessToken;

                FB.api('/me?fields=id,first_name,last_name,name,gender,email,picture', function(response) {
                    console.log(JSON.stringify(response));

                    var datosArray = new Array();
                    datosArray["id"] = response.id;
                    datosArray["name"] = response.first_name;
                    datosArray["lastname"] = response.last_name;
                    datosArray["img"] = response.picture.data.url;
                    datosArray["email"] = response.email;
                    datosArray["token"] = token;
                    datosArray["type"] = "facebook"; //1. local 2.fb 3. gl

                    createAccountUser(datosArray)

                    // revisarDatos(datosArray);

                    // console.log(token);
                    // console.log(datosArray);

                });
            } else {
                document.getElementById('mensaje-login').innerHTML = 'Error login' + response.status;
            }

        }, { scope: 'public_profile,email' });
    });

    FB.getLoginStatus(function(response) {
        //statusChangeCallback(response);
    });

};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/" + idiomaApp + "/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


statusChangeCallback = function(response) {
    console.log("statusChangeCallback:" + response);
    token = response.authResponse.accessToken;
}



function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}