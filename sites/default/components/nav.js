import {
    unDisabledElem,
    pageActive,
    removeModalPage,
    loadLoadingLine,
    removeLoadingLine,
    disabledElem,
    outId,
    validateEmail,
    accessToken,
    alertPage,
    loadView,
    getUrlLocation
} from "./index.js"
import {
    renderMessageCreateAccount,
    renderMessageCreateAddress,
    renderMessageAccountExist,
    renderAddresses,
    renderAddressToList,
    renderSignIn,
    renderSignUp,
} from "./render.js"

// console.log("nav.js");
document.addEventListener("DOMContentLoaded", function () {
    ////Funcionalidades del carrito de compras

    var xpw = ""
    var ypw = ""
    var numCode = 0
    var numPw = 0


    resizeNav()

    //verificateLogin()

    window.addEventListener("resize", resizeNav())

    let pageHome = $(".bodyPage").html()

    $(".pub-activation").addClass("on")

    $("body").on("click", ".btnMenuMobile", function () {
        $(".navNodes").addClass("on")
    })

    $("body").on("click", ".btnCloseMobile", function () {
        $(".navNodes").removeClass("on")
    })

    $("body").on("mouseover", ".navNodes .node", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        let pathurl = $(this).attr("pathurl")
        console.log('.navNodes', pathurl);

        $(".navNodes .node[pathurl='" + pathurl + "'] .subMenu").addClass("on");
        $(".navNodes .node[pathurl='" + pathurl + "'] .nod").addClass("active");

    })

    $("body").on("mouseleave", ".navNodes .node", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        removeMenus();
    })

    $("body").on("click", ".btnRoot", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        console.log('.btnRoot');
        let url = window.location.href;

        if (url != _PATH_WEB) {
            console.log('url != _PATH_WEB');

            loadView(_PATH_WEB + "home?m=react").then((response) => {
                //console.log('loadView', response);
                $(".ws-1").remove();
                $("#bodyIndex").append(response);
                var stateObj = {
                    estado: "ok"
                };
                window.history.pushState(stateObj, 'html5', _PATH_WEB);
                removeMenus();
            }).catch(console.warn());
        }
    });

    $("body").on("click", `.btnSliderStr`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let item = $(this).attr("item");
        let videoSrc = $(`.video[item='${item}'] iframe`).attr("src");
        //console.log(`.btnSliderStr`, item, videoSrc);

        $(".video").removeClass("on");
        $(`.video[item='${item}']`).addClass("on");
        $(`.video[item='${item}'] iframe`).attr("src", videoSrc + "&autoplay=1");

    })

    $("body").on("mouseover", `.btnSliderStr`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let item = $(this).attr("item");
        //console.log(`.btnSliderStr`, );

        $(".slidesjs-slide .img").removeClass("on");
        $(`.slidesjs-slide .img[item='${item}']`).addClass("on");

    })


    $("body").on("click", `.btnCancel`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let item = $(this).attr("item");
        let videoSrc = $(`.video[item='${item}'] iframe`).attr("src");
        let newVideoSrc = videoSrc.replace("&autoplay=1", "");
        //console.log(`.btnCancel`, );
        $(".video").removeClass("on");
        $(`.video[item='${item}'] iframe`).attr("src", newVideoSrc);
    })

    $("body").on("mouseleave", `.btnSliderStr`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let item = $(this).attr("item");

        //console.log(`.btnSliderStr`, );

        $(".slidesjs-slide .img").removeClass("on");


    })

    $("body").on("mouseover", `.btnService`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let path = $(this).attr("path");
        //console.log(`.btnSliderStr`, );

        $(".btnService .summary").removeClass("on");
        $(`.btnService .bg`).removeClass("on");
        $(`.btnService`).removeClass("on");

        $(".summary", this).addClass("on");
        $(`.bg`, this).addClass("on");
        $(this).addClass("on");

    })
    $("body").on("mouseleave", `.btnService`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $(".btnService .summary").removeClass("on");
        $(`.btnService .bg`).removeClass("on");
        $(`.btnService`).removeClass("on");
    })


    $("body").on("mouseover", `.btnPartner`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $(`.btnPartner`).removeClass("on");
        $(this).addClass("on");

    })

    $("body").on("mouseleave", `.btnPartner`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $(".btnPartner").removeClass("on");
    })

    $("body").on("mouseover", `.btnModDirector`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $(`.btnModDirector`).removeClass("on");
        $(this).addClass("on");

    })


    $("body").on("click", `.btnModDirector`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $(`.btnModDirector .video`).removeClass("on");
        $(".video", this).addClass("on");

    })

    $("body").on("mouseleave", `.btnModDirector`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $(".btnModDirector").removeClass("on");
        $(".btnModDirector .video").removeClass("on");
    })

    $("body").on("click", `.btnLastProyects`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data()
        console.log(`.btnLastProyects`, data);
        $(`html,body`).animate({
            scrollTop: $(`.slides`).offset().top
        }, 800);
    })

    $("body").on("click", `.btnDirector`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let path = $(this).attr("path");
        console.log(`.btnDirector`, );
        $(`html,body`).animate({
            scrollTop: $(`.btnModDirector[path='${path}']`).offset().top
        }, 800);
        setTimeout(() => {
            $(`.btnModDirector .video`).removeClass("on");
            $(`.btnModDirector[path='${path}'] .video`).addClass("on");
        }, 1000);

    })
    $("body").on("click", `.btnDirectores`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $(`html,body`).animate({
            scrollTop: $(`#directores`).offset().top
        }, 800);
        console.log(`.btnDirectores`, );

        $(".btnMenuGeneral").removeClass("on");
        $(".navInner").removeClass("on");

    })

    $("body").on("click", `.btnMenuGeneral`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data()
        console.log(`.btnMenuGeneral`, data);
        $(this).toggleClass("on");
        $(".navInner").toggleClass("on");
    })


    $(window).scroll(function () {
        if ($(this).scrollTop() == 0) {
            $('nav').removeClass("on");
            $('.btnMenuGeneral').removeClass("move");

        } else {
            $('nav').addClass("on");
            $('.btnMenuGeneral').addClass("move");

        }
    });
})



//Responsive
export const resizeNav = () => {
    let w = $(window).width()
    let h = $(window).height()
    console.log(w + ":" + h)
    //mobile
    let wNavSearch = $(".navSearch").outerWidth()
    let wBtnService = $(".btnService").outerWidth();
    let hBtnService = wBtnService * 1.6;
    $(".reply").outerWidth(wNavSearch);

    $(".btnService").outerHeight(hBtnService);

    if (w < 470) {
        console.log(w + ":" + h)
        let hSignInner = h - 63
        $("#pageSignIn .inner").outerHeight(hSignInner)
    }
    // tablet
    if (w > 470 && w < 980) {}
    //desktop
    if (w > 980) {}
}

export const removeMenus = (vars) => {
    //console.log('removeMenus');
    $(".navNodes .subMenu").removeClass("on");
    $(".navNodes .node .nod").removeClass("active");
}