import {
    dayDate,
    outElem,
    loadLoadingLine,
    pageActive,
    loadView,
    removeLoadingLine,
    replaceAll,
    unDisabledElem,
    loadBtnLoading,
    removeBtnLoading,
    loadingBtnIcon,
    removeLoadingIcon,
    removeLoading,
    replacePath,
    accessToken,
    loadBtnLoadingElem,
    removeBtnLoadingElem,
} from "./index.js"

import {
    renderItemCart,
    renderMessageCreatePay,
    renderMessageCreateAddress,
    renderMessageCreateInvoice,
    renderMessageCreateAccount,
    renderAddressToList,
    renderMessageNoItem,
    renderAddressCart,
    renderMessageAccountExist,
    renderItemCartPurchase,
} from "./render.js"

let queryCart = {}
let queryCartDelivery = {}
let stateBuy = 0

//Carts Actions
document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", ".btnCart", function (e) {
        e.preventDefault()
        e.stopPropagation()
        outElem(".modalPage")
        loadCartPage()
    })

    $("body").on("click", ".btnCartPage", function (e) {
        e.preventDefault()
        e.stopPropagation()

        loadCartPage()
    })

    if (localStorage.getItem("queryCart")) {
        queryCart = JSON.parse(localStorage.getItem("queryCart"))
        setItemsCart()
        for (let i in queryCart) {
            $("#itemProduct" + queryCart[i].item).val(queryCart[i].quantity)
        }
    }

    $("body").on("click", ".btnItemPlus", function (e) {
        e.preventDefault()
        e.stopPropagation()
        let item = $(this).attr("item")
        let typeBtn = $(this).attr("typebtn")
        let quantity = 1

        if (queryCart[item]) {
            quantity = queryCart[item].quantity + 1
            $("#inputProduct" + item).val(quantity)
            $("#inputProductCart" + item).val(quantity)
        }
        $("#inputProduct" + item).val(quantity)
        $("#inputProductCart" + item).val(quantity)

        setQueryCart(item, quantity)
        setItemsCart()

        if (typeBtn == "btnCart") {
            setCartStep1()
        }
        //$("#itemProduct" + item).val(quantity)
        //loadCartSession({ item: item, quantity: quantity })
    })

    $("body").on("click", ".btnItemMinus", function (e) {
        e.preventDefault()
        e.stopPropagation()
        let item = $(this).attr("item")
        let typeBtn = $(this).attr("typebtn")
        let quantity = 0
        if (queryCart[item] && queryCart[item].quantity > 0) {
            quantity = queryCart[item].quantity - 1
        }
        $("#inputProduct" + item).val(quantity)
        $("#inputProductCart" + item).val(quantity)
        setQueryCart(item, quantity)
        setItemsCart()

        if (typeBtn == "btnCart") {
            setCartStep1()
        }
        //loadCartSession({ item: item, quantity: quantity })
    })

    $("body").on("click", ".btnItemDeleteCart", function (e) {
        e.preventDefault()
        e.stopPropagation()

        const quantity = 0
        let item = $(this).attr("item")

        $("#inputProduct" + item).val(0)
        $("#itemProductCart" + item).addClass("animated bounceOut")
        setTimeout(() => {
            $("#itemProductCart" + item).remove()
        }, 200)

        setQueryCart(item, quantity)
        setItemsCart()

        loadCartPage()
        //loadCartSession({ item: item, quantity: quantity })
    })

    $("body").on("click", ".btnCheckOut", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        loadCheckOutPage()
    })

    $("body").on("keyup", "#inputCelularCheckOut", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        let value = $(this).val()
        //console.log(value)
        $(".menssageCelularCheckOut").html("")
        if (value.length == 8 && parseInt(value)) {
            console.log("celular valido")
            setStateBuy()
        } else {
            if (!parseInt(value)) {
                $(".menssageCelularCheckOut").html("Celular no valido")
            }
            if (value.length > 4) {
                $(".menssageCelularCheckOut").html("Celular no valido")
            }
            if (value.length == 0) {
                $(".menssageCelularCheckOut").html("")
            }
        }
    })

    $("body").on("keyup", "#inputRazonSocial", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        let value = $(this).val()
        setStateBuy()
    })

    $("body").on("keyup", "#inputNit", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        let value = $(this).val()
        if (parseInt(value)) {
            setStateBuy()
        } else {
            $(this).val("")
        }
    })

    $("body").on("click", ".btnPurchase", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()

        setQueryCartDelivery()
        loadPurchasePage()
    })

    $("body").on("click", ".btnPurchasePay", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        loadBtnLoadingElem(".btnPurchasePay")
        const objCart = JSON.parse(localStorage.getItem("queryCartDelivery"))
        let client = JSON.parse(localStorage.getItem("ga_client_id"))
        let obj2 = []

        obj2["products"] = JSON.parse(localStorage.getItem("queryCart"))
        const producto = {
            concept: objCart.concept,
            item: 0,
            price: objCart.price,
            pathImg: "",
            typeMoney: objCart.typeMoney,
            quantity: objCart.quantity,
        }
        obj2["products"][producto.item] = { ...producto }
        // console.log(obj1)
        //console.log(obj2)
        //console.log(Object.assign(objCart, obj2))

        //console.log(obj3)
        createCart(Object.assign(objCart, obj2))
            .then((response) => {
                console.log(response)
                if (response.Error == 0) {
                    if (objCart.paymentMethod === "multipay") {
                        purchasePayLibelula(
                            Object.assign(objCart, obj2, response, client)
                        ).then((response) => {
                            loadPaymentGateway(Object.assign(objCart, response))
                        })
                    }
                    if (objCart.paymentMethod === "cash-on-delivery") {
                        console.log("cash-on-delivery")
                    }
                }
            })
            .catch((error) => {
                console.log(error)
            })
    })
})

//Address Actions
document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", ".btnClearInputSearchAddress", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        $(".inputAddressLabel").val("")
        $(".inputAddressLabel").focus()
        $(".btnClearInputSearchAddress").removeClass("on")
        $(".btnSearchAddress").removeClass("on")
    })

    $("body").on("keyup", "#inputAddressLabel", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        $(".btnClearInputSearchAddress").addClass("on")
        $(".btnSearchAddress").addClass("on")
    })

    $("body").on("click", ".btnAddAddress", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        activeMapAddress()
    })

    $("body").on("click", ".btnAddAddressPlace", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        loadBtnLoading(e)
        const coord = $("#inputCoord").val()
        const inputAddress = $("#inputAddressLabel").val()
        const inputPerimeter = $("#inputPerimeter").val()
        const inputDeliveryCost = $("#inputDeliveryCost").val()

        if (inputPerimeter == 0) {
            $(".messageMap").html(renderPerimetrerMessageNotCovered())
            setTimeout(() => {
                clearInputMap(e)
            }, 3600)
        } else {
            addAddressPlace({
                coord: coord,
                inputAddress: inputAddress,
                inputPerimeter: inputPerimeter,
            }).then((response) => {
                if (response.Error == 0) {
                    deactiveMapAddress(e)
                    addAddressList({
                        address: inputAddress,
                        addId: response.addId,
                        deliveryCost: inputDeliveryCost,
                        perimeter: inputPerimeter,
                        order: response.order,
                    })
                }
            })
        }
    })

    $("body").on("click", ".btnAddressDelete", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        const item = $(this).attr("item")
        activateDeleteAddress(item)
    })

    $("body").on("click", ".btnCancelAddress", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        const item = $(this).attr("item")
        desactivateDeleteAddress(item)
    })

    $("body").on("click", ".btnRemoveAddress", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        const item = $(this).attr("item")
        loadingBtnIcon(".btnRemoveAddress")
        deleteAddress({ item: item }).then((response) => {
            if (response.Error == 0) {
                deleleItemAddress(item)
                removeLoadingIcon(".btnRemoveAddress")
            }
        })
    })

    $("body").on("click", ".btnAddressActive", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        let item = $(this).attr("item")
        let perimeter = $(this).attr("perimeter")
        let address = $(this).attr("address")
        let deliveryCost = $(this).attr("deliveryCost")
        activeAnimationBoxAddress()
        stateAddressCart({ item, perimeter, address })
    })
})

//Carts
export const setCartStep1 = () => {
    loadCartPage()
}

export const setItemsCart = () => {
    let ItemsQuantity = Object.keys(queryCart).length
    localStorage.setItem("dateQuery", dayDate()) //fn index.js

    if (ItemsQuantity) {
        $(".NumberItemsCart").text(ItemsQuantity)
        $(".NumberItemsCart").addClass("on")
    } else {
        $(".NumberItemsCart").text("")
        $(".NumberItemsCart").removeClass("on")
    }
}

export const setQueryCart = (item, quantity) => {
    const dataItem = document.getElementById(`btnProducto${item}`)
    const concept = dataItem.querySelector("h3").textContent
    const pathImg = $(`#btnProducto${item}`).attr("path-img")
    // let precio = dataItem.querySelector(".precio").textContent;
    const price = $(`#btnProducto${item}`).attr("price")
    const typeMoney = $(`#btnProducto${item}`).attr("typemoney")

    const producto = {
        concept: concept,
        item: item,
        price: price,
        pathImg: pathImg,
        typeMoney: typeMoney,
        quantity: quantity,
    }
    queryCart[producto.item] = { ...producto }
    if (producto.quantity === 0) {
        delete queryCart[item]
    }
    console.log(queryCart)
    localStorage.setItem("queryCart", JSON.stringify(queryCart))
}

export const setQueryCartDelivery = () => {
    const perimeters = JSON.parse(localStorage.getItem("perimeters"))
    let perimeterId = $("#inputPerimeterAddress").val()
    let addId = $("#inputAddressId").val()
    let addName = $("#inputAddressName").val()
    let perimetro = perimeters.find((item) => item.id == perimeterId)
    let celular = $("#inputCelularCheckOut").val()
    let razonSocial = $("#inputRazonSocial").val()
    let nit = $("#inputNit").val()
    let details = $("#detailsDelivery").val()
    let paymentMethod = $("input[name='optionsPay']:checked").val()

    //console.log(perimetro.cost)
    queryCartDelivery = {
        concept: "Delivery " + perimetro.name,
        deliveryId: perimeterId,
        price: perimetro.cost,
        typeMoney: _COIN_DEFAULT,
        quantity: 1,
        paymentMethod,
        celular,
        razonSocial,
        nit,
        details,
        addId,
        addName,
    }

    localStorage.setItem("queryCartDelivery", JSON.stringify(queryCartDelivery))
}

export const setStateBuy = () => {
    const addId = $("#inputAddressId").val()
    const celular = $("#inputCelularCheckOut").val()
    const invoice = $("#inputRazonSocial").val()
    const nit = $("#inputNit").val()

    if (addId && celular && invoice && nit) {
        console.log("activar boton")
        activeBtnBuy()
    } else {
        inactiveBtnBuy()
    }
}

export const activeBtnBuy = () => {
    $(".btnPurchase").addClass("animated flash")
    $(".btnPurchase").removeClass("disabled")
}
export const inactiveBtnBuy = () => {
    $(".btnPurchase").removeClass("animated flash")
    $(".btnPurchase").addClass("disabled")
}

export const loadCartPage = () => {
    const dataQuery = localStorage.getItem("queryCart")
    const pageName = "cartPage"
    let totalOrderStr = 0
    $(".cartPage").html("")
    loadLoadingLine(".ws")
    pageActive("." + pageName)
    //console.log(dataQuery)
    if (dataQuery != null && dataQuery != "{}") {
        //console.log("no null")
        loadView(_PATH_WEB + "sites/default/views/cart.html").then((page) => {
            let pageHtml = page
            let totalOrder = 0
            let typeMoneyTotal = "Bs"
            let itemsCart = '<div class="listItems">'
            itemsCart += "<label>Los Items</label>"

            for (let i in queryCart) {
                const item = queryCart[i].item
                const concept = queryCart[i].concept
                const pathImg = queryCart[i].pathImg
                const quantity = queryCart[i].quantity
                const price = queryCart[i].price
                const typeMoney = queryCart[i].typeMoney
                const priceItem = Number.parseFloat(price * quantity).toFixed(2)

                totalOrder =
                    (Number.parseFloat(price) + totalOrder) *
                    Number.parseInt(quantity)

                itemsCart += renderItemCart({
                    item: item,
                    concept: concept,
                    price: typeMoney + ". " + priceItem,
                    quantity: quantity,
                    pathImg: pathImg,
                })
            }
            itemsCart += "</div>"
            totalOrderStr = typeMoneyTotal + ". " + totalOrder.toFixed(2)

            pageHtml = replaceAll(pageHtml, "{{_ITEMS}}", itemsCart)
            pageHtml = replaceAll(pageHtml, "{{_TOTAL_ORDER}}", totalOrderStr)
            pageHtml = replaceAll(pageHtml, "{{_TOTAL}}", totalOrderStr)
            $(".bodyPage ." + pageName).html(pageHtml)
        })
    } else {
        //console.log("si null")
        loadView(_PATH_WEB + "sites/default/views/cart.html").then((page) => {
            let pageHtml = page
            pageHtml = replaceAll(pageHtml, "{{_ITEMS}}", renderMessageNoItem())
            pageHtml = replaceAll(pageHtml, "{{_FORM_TOTAL}}", "formDisabled")
            pageHtml = replaceAll(pageHtml, "{{_TOTAL_ORDER}}", "")
            $(".bodyPage ." + pageName).html(pageHtml)
            removeLoading()
        })
    }
    removeLoadingLine()
}

export const loadPurchasePage = () => {
    const pageName = "cartPage"
    let totalOrderStr = 0
    loadLoadingLine(".ws")
    loadView(_PATH_WEB + "sites/default/views/cartPurchase.html").then(
        (page) => {
            let pageHtml = page
            let totalOrder = 0
            let typeMoneyTotal = "Bs"
            let itemsCart = '<div class="listItems">'
            itemsCart += "<label>Los Items</label>"

            for (let i in queryCart) {
                const item = queryCart[i].item
                const concept = queryCart[i].concept
                const pathImg = queryCart[i].pathImg
                const quantity = queryCart[i].quantity
                const price = queryCart[i].price
                const typeMoney = queryCart[i].typeMoney
                const priceItem = Number.parseFloat(price * quantity).toFixed(2)

                totalOrder =
                    (Number.parseFloat(price) + totalOrder) *
                    Number.parseInt(quantity)

                itemsCart += renderItemCartPurchase({
                    item: item,
                    concept: concept,
                    price: typeMoney + ". " + priceItem,
                    quantity: quantity,
                    pathImg: pathImg,
                })
            }
            //console.log(queryCartDelivery)
            let deliveryCost = queryCartDelivery.price
            let pay = ""
            const finalCost =
                Number.parseFloat(totalOrder) + Number.parseFloat(deliveryCost)
            let detailsText = ""
            if (queryCartDelivery.details == "") {
                detailsText = `<label>Dirección de Entrega:</label><span>${queryCartDelivery.addName}</span>`
            } else {
                detailsText = `<label>Dirección de Entrega:</label><span>${queryCartDelivery.addName}</span><label>Detalles:</label><span>${queryCartDelivery.details}</span>`
            }

            itemsCart += renderItemCartPurchase({
                item: 0,
                concept: queryCartDelivery.concept,
                price: queryCartDelivery.typeMoney + ". " + deliveryCost,
                quantity: queryCartDelivery.quantity,
                pathImg: "sites/default/assets/img/delivery.png",
            })

            itemsCart += "</div>"

            totalOrderStr = typeMoneyTotal + ". " + finalCost.toFixed(2)

            if (queryCartDelivery.paymentMethod == "multipay") {
                pay = "Multipago"
            }
            if (queryCartDelivery.paymentMethod == "cash-on-delivery") {
                pay = "Pago contra Entrega"
            }
            pageHtml = replaceAll(pageHtml, "{{_ITEMS}}", itemsCart)
            pageHtml = replaceAll(pageHtml, "{{_TOTAL_ORDER}}", totalOrderStr)
            pageHtml = replaceAll(pageHtml, "{{_TOTAL}}", totalOrderStr)
            pageHtml = replaceAll(pageHtml, "{{_ADDRESS}}", detailsText)
            pageHtml = replaceAll(pageHtml, "{{_METHOD_PAY}}", pay)
            if (queryCartDelivery.razonSocial) {
                pageHtml = replaceAll(
                    pageHtml,
                    "{{_INVOICE}}",
                    "<label><strong>Factura con credito Fiscal:</strong></label><span>" +
                        queryCartDelivery.razonSocial +
                        "</span><label><strong>NIT:</strong></label><span>" +
                        queryCartDelivery.nit +
                        "</span>"
                )
            }
            pageHtml = replaceAll(
                pageHtml,
                "{{_CELULAR}}",
                queryCartDelivery.celular
            )
            pageHtml = replaceAll(
                pageHtml,
                "{{_DATE_DELIVERY}}",
                "Entre 25min y 45 min."
            )
            pageHtml = replacePath(pageHtml)
            $(".bodyPage ." + pageName).html(pageHtml)
            removeLoadingLine()
        }
    )
}

export const loadCheckOutPage = () => {
    const pageName = "cartPage"
    let totalOrderStr = 0
    loadLoadingLine(".ws")

    loadView(_PATH_WEB + "sites/default/views/cartCheckOut.html").then(
        (page) => {
            let pageHtml = page
            let totalOrder = 0
            let typeMoneyTotal = "Bs"
            const gaClientId = localStorage.getItem("ga_client_id")
            let client = ""
            //console.log(gaClientId)
            if (gaClientId != "") {
                client = JSON.parse(gaClientId)
            }
            // console.log(client)

            let strAccount = ""
            let strAddress = ""
            let strInvoice = ""
            let strPay = renderMessageCreatePay()

            //{{_ACCOUNT}}
            if (client == null || client == "") {
                strAccount = renderMessageCreateAccount()
                strAddress = renderMessageCreateAddress()
                strInvoice = renderMessageCreateInvoice()

                pageHtml = replaceAll(pageHtml, "{{_ACCOUNT}}", strAccount)
                pageHtml = replaceAll(
                    pageHtml,
                    "{{_TOTAL_ORDER}}",
                    _COIN_DEFAULT + ". " + totalOrderPrice()
                )
                pageHtml = replaceAll(pageHtml, "{{_TOTAL}}", totalOrderPrice())
                pageHtml = replaceAll(pageHtml, "{{_ADDRESS}}", strAddress)
                pageHtml = replaceAll(pageHtml, "{{_INVOICE}}", strInvoice)
                pageHtml = replaceAll(pageHtml, "{{_PAY}}", strPay)
                pageHtml = replaceAll(
                    pageHtml,
                    "{{_DATE_DELIVERY}}",
                    "Entre 25min y 45 min."
                )

                $(".bodyPage ." + pageName).html(pageHtml)
            } else {
                strInvoice = renderMessageCreateInvoice()
                strAccount = renderMessageAccountExist(
                    client.userName + " " + client.userLastname
                )

                if (client.addresses == 0) {
                    strAddress = renderMessageCreateAddress()
                } else {
                    strAddress = ""
                }

                pageHtml = replaceAll(pageHtml, "{{_ACCOUNT}}", strAccount)
                pageHtml = replaceAll(
                    pageHtml,
                    "{{_TOTAL_ORDER}}",
                    _COIN_DEFAULT + ". " + totalOrderPrice()
                )
                pageHtml = replaceAll(pageHtml, "{{_TOTAL}}", totalOrderPrice())
                pageHtml = replaceAll(pageHtml, "{{_ADDRESS}}", strAddress)
                pageHtml = replaceAll(pageHtml, "{{_INVOICE}}", strInvoice)
                pageHtml = replaceAll(pageHtml, "{{_PAY}}", strPay)
                pageHtml = replaceAll(
                    pageHtml,
                    "{{_DATE_DELIVERY}}",
                    "Entre 25min y 45 min."
                )

                $(".bodyPage ." + pageName).html(pageHtml)
                $(".boxAccount").removeClass("active")
                if (client.addresses != 0) {
                    stateAddressCart({
                        perimeter: client.addresses[0].perimeterId,
                        item: client.addresses[0].id,
                        name: client.addresses[0].name,
                        address: client.addresses[0].name,
                    })
                }
                if (client.invoiceData != 0) {
                    $("#inputRazonSocial").val(
                        client.invoiceData[0].razonSocial
                    )
                    $("#inputNit").val(client.invoiceData[0].nit)
                }
                if (client.celular != "") {
                    $("#inputCelularCheckOut").val(client.celular)
                }

                if (client.addresses == 0) {
                    unDisabledElem(".btnAddAddresses")
                }
                if (
                    client.addresses != 0 &&
                    client.celular != "" &&
                    client.invoiceData != 0
                ) {
                    unDisabledElem(".btnPurchase")
                }
            }
            removeLoadingLine()
        }
    )
}

export const totalOrderPrice = () => {
    let totalOrder = 0
    let typeMoneyTotal = "Bs"

    for (let i in queryCart) {
        const item = queryCart[i].item
        const quantity = queryCart[i].quantity
        const price = queryCart[i].price
        const typeMoney = queryCart[i].typeMoney
        const priceItem = Number.parseFloat(price * quantity).toFixed(2)

        totalOrder =
            (Number.parseFloat(price) + totalOrder) * Number.parseInt(quantity)
    }

    return totalOrder.toFixed(2)
}

export const createCart = async (vars) => {
    const url =
        _PATH_WEB_NUCLEO +
        "modules/ecommerce/controllers/apis/v1/carts.php?" +
        _VS
    const dataForm = new FormData()
    dataForm.append("accessToken", JSON.stringify(accessToken))
    dataForm.append("action", "createCart")
    dataForm.append("vars", JSON.stringify(vars))

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {
                "Content-Type": "application/json",
            },
            data: dataForm,
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("error createCart")
        console.log(error)
    }
}

// Map & address
export const activeMapAddress = (e) => {
    $(".listAddresses").removeClass("on")
    $(".mapAddress").addClass("on animated fadeInUp")
}

export const deactiveMapAddress = (e) => {
    clearInputMap(e)
    $(".listAddresses").addClass("on")
    $(".mapAddress").removeClass("on animated fadeInUp")
}

export const clearInputMap = (e) => {
    $(".messageMap").html("")
    removeBtnLoading(e)
    unDisabledElem(".btnAddAddressPlace")
    $(".btnClearInputSearchAddress").removeClass("on")
    $("#inputAddressLabel").val("")
    $("#inputAddressLabel").focus()
}

export const addAddressList = (vars) => {
    $(".listAddresses .list").append(renderAddressToList(vars))
}

export const activateDeleteAddress = (item) => {
    $(".removeAddress[item='" + item + "']").addClass(
        "on animated bounceInRight"
    )
    $(".actions[item='" + item + "']").removeClass("on")
}
export const desactivateDeleteAddress = (item) => {
    $(".removeAddress[item='" + item + "']").removeClass(
        "on animated bounceInRight"
    )
    $(".actions[item='" + item + "']").addClass("on")
}

export const deleleItemAddress = (item) => {
    console.log("deleteItemAddress:" + item)
    $(".listAddresses .address[item='" + item + "']").removeClass(
        "animated bounceInRight"
    )
    $(".listAddresses .address[item='" + item + "']").addClass(
        "animated bounceOutRight"
    )
    $(".listAddresses .address[item='" + item + "']").remove()
}

export const stateAddressCart = (vars) => {
    //console.log(vars)
    const perimeters = JSON.parse(localStorage.getItem("perimeters"))
    //console.log(perimeters)
    let perimeter = perimeters.find((item) => item.id == vars.perimeter)
    //console.log(perimeter)

    $("#modalAddresses").remove()
    $(".pub-cart .boxAddress").removeClass("active")

    $(".pub-cart .boxAddress").html(renderAddressCart(vars))
    $("#deliveryCostValue").html(_COIN_DEFAULT + ". " + perimeter.cost)
    const costTotal =
        Number.parseFloat(totalOrderPrice()) + Number.parseFloat(perimeter.cost)
    setQueryCartDelivery()
    $("#totalCart").html(_COIN_DEFAULT + ". " + costTotal.toFixed(2))
    //Añadir costo defivery
    setStateBuy()
    setTimeout(() => {
        desactiveAnimationBoxAddress()
    }, 1200)
}

export const activeAnimationBoxAddress = () => {
    $(".pub-cart .boxAddress").addClass("animated fadeIn infinite")
}

export const desactiveAnimationBoxAddress = () => {
    $(".pub-cart .boxAddress").removeClass("animated fadeIn infinite")
}

export const loadPerimeters = async (vars) => {
    var url =
        _PATH_WEB_NUCLEO + "modules/logistics/controllers/apis/v1/logistics.php"
    const dataForm = new FormData()
    dataForm.append("access_token", localStorage.getItem("access_token"))
    dataForm.append("refresh_token", localStorage.getItem("refresh_token"))
    dataForm.append("entitieId", localStorage.getItem("idEntitie"))
    dataForm.append("page", _PATH_PAGE)

    dataForm.append("actions", "loadPerimeters")

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })
    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.text()
        //console.log(str)

        //console.log('loadPerimeters: ' + str)
        return str
    } catch (err) {
        console.log("loadPerimeters Error: " + err)
    }
}

export const addAddressPlace = async (vars) => {
    var vari = JSON.stringify(vars)
    var url =
        _PATH_WEB_NUCLEO + "modules/logistics/controllers/apis/v1/logistics.php"
    const dataForm = new FormData()
    dataForm.append("access_token", localStorage.getItem("access_token"))
    dataForm.append("refresh_token", localStorage.getItem("refresh_token"))
    dataForm.append("entitieId", localStorage.getItem("idEntitie"))
    dataForm.append("page", _PATH_PAGE)

    dataForm.append("actions", "addAddressPlace")
    dataForm.append("vars", vari)

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })
    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.json()
        console.log(str)
        //console.log(': ' + str)
        return str
    } catch (err) {
        console.log(" Error: " + err)
    }
}

export const deleteAddress = async (vars) => {
    var vari = JSON.stringify(vars)
    var url =
        _PATH_WEB_NUCLEO + "modules/logistics/controllers/apis/v1/logistics.php"
    const dataForm = new FormData()
    dataForm.append("access_token", localStorage.getItem("access_token"))
    dataForm.append("refresh_token", localStorage.getItem("refresh_token"))
    dataForm.append("entitieId", localStorage.getItem("idEntitie"))
    dataForm.append("page", _PATH_PAGE)

    dataForm.append("actions", "deleteAddress")
    dataForm.append("vars", vari)

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })
    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.json()
        console.log(str)
        //console.log('removeAddress: ' + str)
        return str
    } catch (err) {
        console.log("removeAddress Error: " + err)
    }
}

export const loadPaymentGateway = (vars) => {
    updateCartGateway(vars).then((response) => {
        console.log(response)
        $(".pub-cart .container").addClass("pay")
        loadView(_PATH_WEB + "sites/default/views/cartPay.html?" + _VS).then(
            (htmlView) => {
                let pageHtml = replaceAll(
                    htmlView,
                    "{{_BODY}}",
                    `<iframe class="responsive-iframe" src="${response.url_pasarela_pagos}"></iframe>`
                )
                $(".pub-cart").html(pageHtml)
            }
        )
    })
}

export const updateCartGateway = async (vars) => {
    const url =
        _PATH_WEB_NUCLEO +
        "modules/ecommerce/controllers/apis/v1/carts.php?" +
        _VS
    const dataForm = new FormData()
    dataForm.append("accessToken", JSON.stringify(accessToken))
    dataForm.append("action", "updateCartGateway")
    dataForm.append("vars", JSON.stringify(vars))

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: dataForm,
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("error updateCartGateway")
        console.log(error)
    }
}

export const purchasePayLibelula = async (vars) => {
    const url = "https://api.todotix.com/rest/deuda/registrar"

    let dataForm = new FormData()

    //console.log(vars)
    //console.log(vars.products)

    let products = new Array()
    let prod = Object.values(vars.products)

    for (let i = 0; i < prod.length; i++) {
        const elm = prod[i]
        const pr = {
            concepto: elm.concept,
            codigo_producto: elm.id,
            costo_unitario: elm.price,
            cantidad: elm.quantity,
        }
        products[i] = { ...pr }
    }

    //console.log(products)

    let data = JSON.stringify({
        appkey: "11bb10ce-68ba-4af1-8eb7-4e6624fed729",
        email_cliente: vars.email,
        identificador: vars.id_transaccion,
        callback_url:"http://farmaline.com.bo/sites/default/controllers/apis/v1/callback.php?code=" +
            vars.code+"&action=callback",
        url_retorno: _PATH_WEB + "?action=stateDelivery&code=" + vars.code,
        descripcion: "Pago Compra Online Farmaline",
        nombre_cliente: vars.userName,
        apellido_cliente: vars.userLastname,
        numero_documento: "4735799",
        tipo_factura: "Productos",
        razón_social: "Farmaline SRL",
        emite_factura: "true",
        tipo_factura: "Productos",
        lineas_detalle_deuda: products,
        lineas_metadatos: [
            {
                nombre: "Vendedor",
                dato: "Portal Web",
            },
            {
                nombre: "Farmaline Online",
                dato: "Tienda Virtual 001",
            },
        ],
    })

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {
                "Content-Type": "application/json",
            },
            data: data,
        })
        let str = res.data
        console.log(res.data)
        setTimeout(() => {
            
        }, 3000);
        return Object.assign(str, { gatewayPayment: "libelula" })
    } catch (error) {
        console.log("error purchasePayLibelula")
        console.log(error)
    }
}
