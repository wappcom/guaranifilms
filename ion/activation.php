<?php

$header = file_get_contents(_PATH_HOST . "sites/default/views/headerSingle.html");
$header =  str_replace("{{title}}", $this->fmt->options->getValue("site_title"), $header);
$header =  str_replace("{{urlFavicon}}", _PATH_WEB . $this->fmt->options->getValue("site_favicon"), $header);
$header =  str_replace("{{_BEARER_TOKEN}}", $this->fmt->options->getValue("bearer_token"), $header);
$header =  str_replace("{{_CLIENT_ID}}", $this->fmt->options->getValue("client_id"), $header);
$header =  str_replace("{{_CLIENT_SECRET}}", $this->fmt->options->getValue("client_secret"), $header);
$header =  str_replace("{{_ID_ENTITIE}}", $this->fmt->options->getValue("entitie_id_default"), $header);
$header =  str_replace("{{_VS}}", "v" . $this->fmt->options->versionPlus(), $header);

$html = $this->fmt->setUrlNucleo($header);
$html = str_replace("{{_PATH_DASHBOARD}}", "sites/default/",$html);

$scriptConfig .= '    <script type="text/javascript" language="javascript"> 
        var tokenAccount = "'. $tokenAuth.'"
    </script>';
 


$html = str_replace("{{_META}}", $scriptConfig, $html);

$html .= file_get_contents(_PATH_HOST . "sites/default/views/body.html");
$htmlAct = file_get_contents(_PATH_HOST . "sites/default/views/activation.html");
$html .= $this->fmt->setUrlNucleo($htmlAct);

$html .= file_get_contents(_PATH_HOST . "sites/default/views/footer.html");


echo $html;
